/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    PUBLIC_API_URL: process.env.PUBLIC_API_URL,
    SHAREPOINT_URL: process.env.SHAREPOINT_URL,
    NEXTAUTH_URL: process.env.NEXTAUTH_URL,
  },
  experimental: {
    appDir: true,
  },
  images: { domains: ["ui-avatars.com", "localhost"] },
  output: "standalone",
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    config.externals.push({
      "utf-8-validate": "commonjs utf-8-validate",
      bufferutil: "commonjs bufferutil",
    });
    return config;
  },
};

module.exports = nextConfig;
