"use client";

import { Save } from "@/interfaces/save.interface";
import * as moment from "moment";
import DownloadButton from "./DownloadButton";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";

type ComponentProps = {
  save: Save;
  bdExts: BdExt[];
};

export default function ExportRow({ save, bdExts }: ComponentProps) {
  const fileMetadatas = (save.fileMetadatas || []).filter(
    (file) => file.ignore !== true
  );
  return (
    <tr>
      <td>{save.id}</td>
      <td>{save.localFolder?.name}</td>
      <td>
        {moment.unix(parseInt(save.timestamp) / 1000).format("MM/DD/YYYY")}
      </td>
      <td>{save.email}</td>
      <td>{fileMetadatas.length}</td>
      <td>
        <DownloadButton save={save} bdExts={bdExts} />
      </td>
    </tr>
  );
}
