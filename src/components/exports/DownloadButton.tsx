import { CreateExportFileDto } from "@/dtos/create-export-file.dto";
import { CreateExportPackageDto } from "@/dtos/create-export-package.dto";
import { PackageDto } from "@/dtos/package.dto";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { Save } from "@/interfaces/save.interface";
import { ColumnsTransformation } from "@/utils/columns-transformation";
import { downloadCsv } from "@/utils/download-csv";
import { stringify } from "csv-stringify/sync";
import { useEffect } from "react";
import { Save as SaveIcon } from "react-feather";

type ComponentProps = {
  save: Save;
  bdExts: BdExt[];
};

export default function DownloadButton({ save, bdExts }: ComponentProps) {
  const handleClick = () => {
    if (!save.fileMetadatas || !save.localFolder) return;
    const bdExt = bdExts.find((bdExt) => bdExt.iso === save.iso);
    if (!bdExt) return;
    const packages = (save.packages || []).map(
      (pck) => new PackageDto("", pck)
    );
    const toExportFiles = save.fileMetadatas.filter((file) => !file.ignore);
    const exportedFiles = toExportFiles.map((toExportFile) => {
      return new CreateExportFileDto(toExportFile, bdExt, save.iso);
    });

    const output = stringify(exportedFiles, {
      header: true,
      columns: ColumnsTransformation,
    });

    const exportedPackages = packages.map(
      (packageDto) => new CreateExportPackageDto(packageDto)
    );

    const outputPackages = stringify(exportedPackages, {
      header: true,
    });

    const timestamp = Date.now();
    downloadCsv(output, `OMEKA-${save.localFolder.name}-${timestamp}`);
    if (exportedPackages.length > 0) {
      downloadCsv(
        outputPackages,
        `PACKAGES-${save.localFolder.name}-${timestamp}`
      );
    }
  };
  return (
    <button className={"btn btn-primary btn-sm"} onClick={handleClick}>
      <SaveIcon />
    </button>
  );
}
