"use client";

import { Save } from "@/interfaces/save.interface";
import { fetchExports } from "@/lib/fetch-exports";
import { useEffect, useState } from "react";
import ExportRow from "./ExportRow";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { fetchAllBdExt } from "@/lib/fetch-all-bd-ext";

export default function ExportList() {
  const [exports, setExports] = useState<Save[]>([]);
  const [bdExts, setBdExts] = useState<BdExt[]>([]);

  useEffect(() => {
    async function getAllBdExt() {
      setBdExts(await fetchAllBdExt());
    }
    async function getExports() {
      setExports(await fetchExports());
    }
    getAllBdExt();
    getExports();
  }, []);

  return (
    <table className="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Date</th>
          <th>Email</th>
          <th>Items</th>
          <th>Download</th>
        </tr>
      </thead>
      <tbody>
        {exports.map((save, i) => (
          <ExportRow key={i} save={save} bdExts={bdExts} />
        ))}
      </tbody>
    </table>
  );
}
