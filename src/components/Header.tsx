"use client";
import { useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useState, useEffect } from "react";
import { ToastContainer } from "react-toastify";

export default function Header() {
  const [avatarSrc, setAvatarSrc] = useState("/avatar-placeholder.png");
  const { data: session } = useSession();
  const pathname = usePathname();

  useEffect(() => {
    console.log(session);
    if (session && session.user && session.user.image) {
      setAvatarSrc(session.user.image);
    } else if (session && session.user && session.user.name) {
      setAvatarSrc(
        `https://ui-avatars.com/api/?name=${encodeURI(
          session.user.name
        )}&size=75`
      );
    }
  }, [session]);

  return (
    <header>
      <nav className="navbar navbar-expand-lg bg-body-tertiary">
        <div className="container-fluid">
          <Link className="navbar-brand" href="/">
            <Image
              src={"/BSF_Logo.png"}
              alt="BSF logo"
              width={235}
              height={72}
            />
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link
                  href={"/"}
                  className={[
                    "nav-link",
                    pathname === "/" ? "active" : "",
                  ].join(" ")}
                >
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  href={"/exports"}
                  className={[
                    "nav-link",
                    pathname === "/exports" ? "active" : "",
                  ].join(" ")}
                >
                  Exports
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  href={"/omeka-export"}
                  className={[
                    "nav-link",
                    pathname === "/omeka-export" ? "active" : "",
                  ].join(" ")}
                >
                  Omeka Export
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  href={"/documentation"}
                  className={[
                    "nav-link",
                    pathname === "/documentation" ? "active" : "",
                  ].join(" ")}
                >
                  Documentation
                </Link>
              </li>
            </ul>
            <div className="d-flex">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0 dropstart">
                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle dropstart"
                    href="#"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <Image
                      src={avatarSrc}
                      width="75"
                      height="75"
                      alt="avatar"
                      className="rounded-circle"
                    />
                  </a>
                  <ul className="dropdown-menu dropstart">
                    <li>
                      <h6 className="dropdown-header">
                        {session?.user?.name || "Error"}
                      </h6>
                    </li>
                    <li>
                      <hr className="dropdown-divider" />
                    </li>
                    <li>
                      <Link href="/api/auth/signout" className="dropdown-item">
                        Signout
                      </Link>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
}
