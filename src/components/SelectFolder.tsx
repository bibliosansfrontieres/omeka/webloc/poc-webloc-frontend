"use client";
import { LocalFolder } from "@/interfaces/local-folder.interface";
import { fetchFolders } from "@/lib/fetch-folders";
import { useRouter } from "next/navigation";
import { ChangeEvent, FormEvent, useEffect, useState } from "react";

type ComponentProps = {};

function SelectFolder({}: ComponentProps) {
  const [folders, setFolders] = useState<LocalFolder[]>([]);
  const [selectedOption, setSelectedOption] = useState<string>("");

  const router = useRouter();

  useEffect(() => {
    async function getFolders() {
      const newFolders = await fetchFolders();
      if (newFolders) {
        setFolders(newFolders);
      }
    }
    getFolders();
    const interval = setInterval(getFolders, 10000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  const handleChange = (e: ChangeEvent<HTMLSelectElement>) => {
    setSelectedOption(e.target.value);
  };

  const handleFormSubmit = (
    e: FormEvent<HTMLFormElement | HTMLButtonElement>
  ) => {
    e.preventDefault();
    if (selectedOption !== "") router.push(`/folder/${selectedOption}`);
  };

  return (
    <div className="row">
      <div className="col">
        <form onSubmit={handleFormSubmit}>
          <div className="mb-3">
            <select
              onChange={handleChange}
              defaultValue={selectedOption}
              className="form-select"
            >
              <option value="">Select a project</option>
              {folders.map((localFolder, index) => {
                return (
                  <option key={index} value={localFolder.id}>
                    {localFolder.name}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="d-grid gap-2">
            <button
              type="submit"
              onSubmit={handleFormSubmit}
              className="btn btn-primary"
            >
              Select
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default SelectFolder;
