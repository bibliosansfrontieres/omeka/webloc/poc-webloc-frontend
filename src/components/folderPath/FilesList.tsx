"use client";
import { forwardRef, useImperativeHandle, useState } from "react";
import { LocalFolder } from "@/interfaces/local-folder.interface";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { Audience } from "@/interfaces/bd-ext/audience.interface";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { License } from "@/interfaces/bd-ext/license.interface";
import ImportSave from "./FilesList/ImportSave";
import FilesTable from "./FilesList/FilesTable";
import SaveButton from "./FilesList/SaveButton";
import ExportButton from "./FilesList/ExportButton";
import LanguageSelection from "./FilesList/LanguageSelection";
import { postSave } from "@/lib/post-save";
import { useSession } from "next-auth/react";
import { LanguageList } from "@/interfaces/bd-ext/languages-list.interface";
import PackagesTable from "./FilesList/PackagesTable";
import { PackageDto } from "@/dtos/package.dto";
import { FolderStatus } from "@/interfaces/folder-status.interface";
import { CornerLeftUp } from "react-feather";

type ComponentProps = {
  folder: LocalFolder;
  audiences: Audience[];
  languages: LanguageList;
  licenses: License[];
  folderStatus: FolderStatus;
};

function FilesList(
  { folder, audiences, languages, licenses, folderStatus }: ComponentProps,
  ref: any
) {
  const defaultFiles = folder.localFiles.map(
    (localFile) => new FileMetadataDto(localFile)
  );
  const [files, setFiles] = useState(defaultFiles);

  const [bdExt, setBdExt] = useState<BdExt | undefined>(undefined);
  const [packages, setPackages] = useState<PackageDto[]>([]);
  const [saves, setSaves] = useState(folder.saves);
  const [iso, setIso] = useState<string | undefined>(undefined);
  const [isDataChanged, setIsDataChanged] = useState(false);

  useImperativeHandle(ref, () => ({
    autoSave,
    isDataChanged,
    files,
    setFiles,
    iso,
  }));
  const { data: session } = useSession();

  const autoSave = async () => {
    if (isDataChanged && iso && session && session.user && session.user.email) {
      const save = await postSave(
        "",
        iso,
        folder,
        files,
        packages,
        session.user.email
      );
      const actualSaves = [...saves];
      const newSaves = actualSaves.filter((save) => save.email === null);
      newSaves.push(save);
      setSaves(newSaves);
      setIsDataChanged(false);
    }
  };

  const handleSetFile = (file: FileMetadataDto, index: number) => {
    setIsDataChanged(true);
    setFiles((files) => {
      const newFiles = [...files];
      newFiles[index] = file;
      return newFiles;
    });
  };

  const handleSetPackage = (packageDto: PackageDto) => {
    setIsDataChanged(true);
    setPackages((packages) => {
      const packageIndex = packages.findIndex(
        (pck) => pck.id === packageDto.id
      );
      if (packageIndex === -1) return packages;
      const newPackages = [...packages];
      newPackages[packageIndex] = packageDto;
      return newPackages;
    });
  };

  const handleAddPackage = (iso: string) => {
    setPackages((packages) => {
      const newPackages = [...packages, new PackageDto(iso)];
      console.log(newPackages);
      return newPackages;
    });
  };

  const handleDeletePackage = (id: string) => {
    setPackages((packages) => {
      const newPackages = [...packages];
      const packageIndex = packages.findIndex((pck) => pck.id === id);
      if (packageIndex === -1) return packages;
      newPackages.splice(packageIndex, 1);
      return newPackages;
    });
    let changeDetected = false;
    const newFiles = [...files];
    for (const file of newFiles) {
      if (file.package === id) {
        changeDetected = true;
        file.package = "";
      }
    }
    if (changeDetected) {
      setFiles(newFiles);
    }
  };

  const isReady = () => {
    const folderReady = folderStatus.status === "ready";
    const filesNotReady = folderStatus.localFiles.filter(
      (localFile) => localFile.status === "not-ready"
    );
    return folderReady && filesNotReady.length === 0;
  };

  const statusColor = isReady() ? "text-success" : "text-danger";
  const statusString = isReady() ? "Ready to export" : "Not ready to export";

  return (
    <>
      <h1 className="mb-3">{folder.name}</h1>
      <p className="mb-3 fs-5">
        Status : <span className={statusColor}>{statusString}</span>
      </p>
      <p>
        The language that you need to select, is the language of the tags,
        categories, geography and temporality.
      </p>
      <div className="mb-2" style={{ display: "flex" }}>
        <LanguageSelection
          iso={iso}
          setBdExt={setBdExt}
          setIso={setIso}
          files={files}
          setFiles={setFiles}
          languages={languages}
        />
        <ImportSave
          saves={saves}
          files={files}
          setFiles={setFiles}
          setBdExt={setBdExt}
          iso={iso}
          setIso={setIso}
          packages={packages}
          setPackages={setPackages}
        />
      </div>
      {iso && bdExt ? (
        <>
          <FilesTable
            folderStatus={folderStatus}
            files={files}
            setFile={handleSetFile}
            bdExt={bdExt}
            languages={languages}
            licenses={licenses}
            audiences={audiences}
            packages={packages}
          />
          <PackagesTable
            files={files}
            packages={packages}
            setPackage={handleSetPackage}
            addPackage={handleAddPackage}
            deletePackage={handleDeletePackage}
            iso={iso}
            bdExt={bdExt}
            audiences={audiences}
            languages={languages}
          />
          <div className="d-flex justify-content-between">
            <SaveButton
              folder={folder}
              packages={packages}
              saves={saves}
              setSaves={setSaves}
              iso={iso}
              files={files}
            />
            <ExportButton
              packages={packages}
              folder={folder}
              iso={iso}
              files={files}
              bdExt={bdExt}
            />
          </div>
        </>
      ) : (
        <div className="mb-4 bg-body-tertiary rounded-3">
          <div className="container-fluid py-3">
            <h2 className="fs-4" style={{ display: "flex" }}>
              <CornerLeftUp
                style={{ marginTop: "2px", marginRight: "0.5em" }}
              />
              <span>
                In order to fill metadata of your files, you need to select a
                language or import a save !
              </span>
            </h2>
          </div>
        </div>
      )}
    </>
  );
}

export default forwardRef(FilesList);
