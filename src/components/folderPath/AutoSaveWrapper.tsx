"use client";
import { Audience } from "@/interfaces/bd-ext/audience.interface";
import { License } from "@/interfaces/bd-ext/license.interface";
import { LocalFolder } from "@/interfaces/local-folder.interface";
import { Dispatch, SetStateAction, useEffect, useRef, useState } from "react";
import FilesList from "./FilesList";
import { LanguageList } from "@/interfaces/bd-ext/languages-list.interface";
import io, { Socket } from "socket.io-client";
import { FolderStatus } from "@/interfaces/folder-status.interface";
import { errorToast } from "../toasts/error";
import { successToast } from "../toasts/success";
import { LocalFile } from "@/interfaces/local-file.interface";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { ItemType } from "@/interfaces/item-type.enum";

type ComponentProps = {
  folder: LocalFolder;
  audiences: Audience[];
  languages: LanguageList;
  licenses: License[];
};

export type AutoSaveRef = {
  setFiles: Dispatch<SetStateAction<FileMetadataDto[]>>;
  files: FileMetadataDto[];
  autoSave: () => void;
  isDataChanged: boolean;
  iso: string | undefined;
};

export default function AutoSaveWrapper({
  folder,
  audiences,
  languages,
  licenses,
}: ComponentProps) {
  const [folderStatus, setFolderStatus] = useState<FolderStatus>({
    status: "not-ready",
    localFiles: [],
  });
  const [socket, setSocket] = useState<Socket | undefined>(undefined);
  const autoSaveRef = useRef<AutoSaveRef>();

  useEffect(() => {
    const interval = setInterval(checkAutoSave, 5000);
    return () => {
      if (interval) {
        clearInterval(interval);
      }
    };
  });

  useEffect(() => {
    async function socketInitializer() {
      setSocket(io(process.env.PUBLIC_API_URL || ""));
    }
    socketInitializer();
    return () => {
      setSocket(undefined);
    };
  }, []);

  useEffect(() => {
    if (socket) {
      socket.on("connect", () => {
        socket.emit("getInformations", { id: folder.id });
      });
      socket.on("disconnect", () => {
        setFolderStatus({ ...folderStatus, status: "not-ready" });
      });
      socket.on(
        `localFolderInformations#${folder.id}`,
        (informations: FolderStatus, localFiles: LocalFile[]) => {
          if (autoSaveRef.current) {
            let changed = false;
            const actualFiles = [...autoSaveRef.current.files];
            for (let i = 0; i < actualFiles.length; i++) {
              const actualFile = actualFiles[i];
              const newFile = localFiles.find(
                (localFile) => localFile.id === actualFile.localFile.id
              );
              if (!newFile) {
                actualFiles.splice(i, 1);
                changed = true;
                i--;
                continue;
              }
              if (actualFile.localFile.type === ItemType.TEXT) {
                if (
                  newFile.size !== null &&
                  newFile.size !== "" &&
                  actualFile.size === ""
                ) {
                  changed = true;
                  actualFiles[i].size = newFile.size;
                }
              } else {
                if (
                  newFile.duration !== null &&
                  newFile.duration !== "" &&
                  actualFile.duration === ""
                ) {
                  changed = true;
                  actualFiles[i].duration = newFile.duration;
                }
                if (
                  newFile.size !== null &&
                  newFile.size !== "" &&
                  actualFile.size === ""
                ) {
                  changed = true;
                  actualFiles[i].size = newFile.size;
                }
              }
            }
            for (let i = 0; i < localFiles.length; i++) {
              const localFile = localFiles[i];
              const actualFile = actualFiles.find(
                (file) => file.localFile.id === localFile.id
              );
              if (!actualFile) {
                changed = true;
                const fileMetadataDto = new FileMetadataDto(localFile);
                if (autoSaveRef.current.iso) {
                  fileMetadataDto.language = autoSaveRef.current.iso;
                }
                actualFiles.push(fileMetadataDto);
              }
            }
            if (changed) {
              autoSaveRef.current.setFiles(actualFiles);
            }
          }
          setFolderStatus(informations);
        }
      );
    }
    return () => {
      if (socket) {
        socket.off(`localFolderInformations#${folder.id}`);
        socket.off("disconnect");
        socket.off("connect");
      }
    };
  }, [socket, folderStatus, folder.id]);

  const checkAutoSave = () => {
    if (autoSaveRef && autoSaveRef.current) {
      if (autoSaveRef.current.isDataChanged) {
        autoSaveRef.current.autoSave();
      }
    }
    if (socket) {
      socket.emit("getInformations", { id: folder.id });
    }
  };

  return (
    <>
      <FilesList
        folder={folder}
        audiences={audiences}
        languages={languages}
        licenses={licenses}
        ref={autoSaveRef}
        folderStatus={folderStatus}
      />
    </>
  );
}
