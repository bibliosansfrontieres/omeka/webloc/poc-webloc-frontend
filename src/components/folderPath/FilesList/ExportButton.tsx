"use client";

import { errorToast } from "@/components/toasts/error";
import { CreateExportFileDto } from "@/dtos/create-export-file.dto";
import { CreateExportPackageDto } from "@/dtos/create-export-package.dto";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { PackageDto } from "@/dtos/package.dto";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { LocalFolder } from "@/interfaces/local-folder.interface";
import { fetchCanExport } from "@/lib/fetch-can-export";
import { postExport } from "@/lib/post-export";
import { ColumnDisabling, columnsDisabling } from "@/utils/columns-disabling";
import { ColumnsTransformation } from "@/utils/columns-transformation";
import {
  columnsValidations,
  ColumnValidation,
} from "@/utils/columns-validation";
import { downloadCsv } from "@/utils/download-csv";
import { stringify } from "csv-stringify/sync";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { LogOut } from "react-feather";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";

type ComponentProps = {
  folder: LocalFolder;
  files: FileMetadataDto[];
  packages: PackageDto[];
  iso: string;
  bdExt: BdExt;
};

export default function ExportButton({
  folder,
  files,
  iso,
  packages,
  bdExt,
}: ComponentProps) {
  const [modalOpen, setModalOpen] = useState<boolean>(false);

  const modalToggle = () => setModalOpen(!modalOpen);

  const router = useRouter();
  const { data: session } = useSession();

  const validations = new Map<string, ColumnValidation>();
  for (const columnValidation of columnsValidations) {
    validations.set(columnValidation.name, columnValidation);
  }
  const disabledCols = new Map<string, ColumnDisabling>();
  for (const disabledCol of columnsDisabling) {
    disabledCols.set(disabledCol.name, disabledCol);
  }

  const checkFields = (): boolean => {
    let isValid = true;
    for (const file of files) {
      if (!file.ignore) {
        for (const field of Object.keys(file)) {
          const disabled = disabledCols.get(field);
          if (!disabled || !disabled.isDisabled(file)) {
            const validation = validations.get(field);
            if (validation && validation.isValid) {
              const fieldIsValid = validation.isValid(file[field], file);
              if (!fieldIsValid.valid) {
                console.log(field);
                isValid = false;
              }
            }
          }
        }
      }
    }
    return isValid;
  };

  const handleExport = async () => {
    const isValid = checkFields();
    if (!isValid) {
      errorToast(
        "Some fields are not valid. Please check if you have any red field."
      );
      return;
    }

    const canExport = await fetchCanExport(folder.id);
    if (!canExport.canExport) {
      errorToast(
        "Project folder is not ready to export !\nPlease wait that the WebLOC finished download files from Sharepoint."
      );
      return;
    }
    modalToggle();
  };

  const handleConfirmedExport = async () => {
    const exportFolder = await postExport(
      folder.id,
      iso,
      files,
      packages,
      session?.user?.email || ""
    );

    if (!exportFolder) {
      errorToast("Error while exporting folder !");
      return;
    }
    const toExportFiles = files.filter((file) => !file.ignore);
    const exportedFiles = toExportFiles.map((toExportFile) => {
      return new CreateExportFileDto(toExportFile, bdExt, iso);
    });

    const output = stringify(exportedFiles, {
      header: true,
      columns: ColumnsTransformation,
    });

    const exportedPackages = packages.map(
      (packageDto) => new CreateExportPackageDto(packageDto)
    );

    const outputPackages = stringify(exportedPackages, {
      header: true,
    });

    const timestamp = Date.now();
    downloadCsv(output, `OMEKA-${folder.name}-${timestamp}`);
    if (exportedPackages.length > 0) {
      downloadCsv(outputPackages, `PACKAGES-${folder.name}-${timestamp}`);
    }
    router.push("/exports");
  };

  const modal = (
    <Modal isOpen={modalOpen} toggle={modalToggle}>
      <ModalHeader toggle={modalToggle}>Warning !</ModalHeader>
      <ModalBody>
        Your are exporting all metadata for Omeka.
        <br />
        When metadata is exported, you won&apos;t be able to edit it anymore.
      </ModalBody>
      <ModalFooter>
        <Button color="success" onClick={handleConfirmedExport}>
          <LogOut /> Export for Omeka
        </Button>
        <Button color="danger" onClick={modalToggle}>
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );

  return (
    <>
      {modal}
      <button className="btn btn-primary" onClick={handleExport}>
        <LogOut /> Export for Omeka
      </button>
    </>
  );
}
