"use client";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { LanguageList } from "@/interfaces/bd-ext/languages-list.interface";
import { fetchBdExt } from "@/lib/fetch-bd-ext";
import {
  ChangeEvent,
  Dispatch,
  SetStateAction,
  useEffect,
  useState,
} from "react";
import { Globe } from "react-feather";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";

type ComponentProps = {
  iso: string | undefined;
  setIso: Dispatch<SetStateAction<string | undefined>>;
  files: FileMetadataDto[];
  setFiles: (files: FileMetadataDto[]) => void;
  setBdExt: Dispatch<SetStateAction<BdExt | undefined>>;
  languages: LanguageList;
};

const buttonTextChange = "Change language";
const buttonTextSet = "Set language";
const buttonTextLoading = "Loading...";

export default function LanguageSelection({
  iso,
  setIso,
  setBdExt,
  files,
  setFiles,
  languages,
}: ComponentProps) {
  const [buttonDisabled, setButtonDisabled] = useState<boolean>(true);
  const [buttonText, setButtonText] = useState<string>(buttonTextSet);
  const [language, setLanguage] = useState<string>(iso || "");
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const toggle = () => setModalIsOpen(!modalIsOpen);

  useEffect(() => {
    if (iso) setLanguage(iso);
  }, [iso]);

  const handleLanguageChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const value = e.target.value;
    if ((value === iso || value === "") && !buttonDisabled) {
      setButtonDisabled(true);
    } else if (value !== iso && buttonDisabled) {
      setButtonDisabled(false);
    }
    setLanguage(value);
  };

  const needConfirmation = () => {
    let askConfirmation = false;
    const newFilesMetadata = [...files];
    for (let i = 0; i < newFilesMetadata.length; i++) {
      const actualLanguage = newFilesMetadata[i].language;
      if (actualLanguage.includes(",")) askConfirmation = true;
      newFilesMetadata[i] = { ...newFilesMetadata[i], language };
    }
    return askConfirmation;
  };

  const handleButtonClick = async () => {
    if (language === "" || language === iso) return;
    setButtonText(buttonTextLoading);
    const askConfirmation = needConfirmation();
    if (askConfirmation) {
      setModalIsOpen(true);
      return;
    }
    await handleConfirm();
  };

  const handleConfirm = async () => {
    const bdExt = await fetchBdExt(language);
    const newFilesMetadata = [...files];
    for (let i = 0; i < newFilesMetadata.length; i++) {
      newFilesMetadata[i] = { ...newFilesMetadata[i], language };
    }
    setBdExt(bdExt);
    setFiles(newFilesMetadata);
    setIso(language);
    if (modalIsOpen) toggle();
    setButtonText(buttonTextChange);
    setButtonDisabled(true);
  };

  const modal = (
    <Modal isOpen={modalIsOpen} toggle={toggle}>
      <ModalHeader toggle={toggle}>Warning !</ModalHeader>
      <ModalBody>
        Some items have multiple languages or have not the same languages then
        the others.
        <br />
        You will override all items language by clicking on Confirm button.
        <br />
      </ModalBody>
      <ModalFooter>
        <Button color="success" onClick={handleConfirm}>
          Confirm
        </Button>
        <Button
          color="danger"
          onClick={() => {
            toggle();
            setButtonText(buttonTextChange);
          }}
        >
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );

  return (
    <>
      <div className="row mb-3 g-2" style={{ marginRight: "1em" }}>
        <div className="col-auto">
          <select
            className="form-select"
            onChange={handleLanguageChange}
            value={language}
          >
            <option value="">Select a language</option>
            {languages.languages.map((language, i) => (
              <option value={language.iso} key={i}>
                {language.name} ({language.iso})
              </option>
            ))}
          </select>
        </div>
        <div className="col-auto">
          <button
            className="btn btn-warning"
            onClick={handleButtonClick}
            disabled={buttonDisabled}
          >
            <Globe /> {buttonText}
          </button>
        </div>
      </div>
      {modal}
    </>
  );
}
