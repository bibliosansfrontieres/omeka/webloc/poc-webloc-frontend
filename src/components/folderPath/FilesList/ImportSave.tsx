"use client";
import { errorToast } from "@/components/toasts/error";
import { successToast } from "@/components/toasts/success";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { Save } from "@/interfaces/save.interface";
import { fetchBdExt } from "@/lib/fetch-bd-ext";
import { fetchSave } from "@/lib/fetch-save";
import moment from "moment";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import {
  ChangeEvent,
  Dispatch,
  MouseEvent,
  SetStateAction,
  useState,
} from "react";
import { LogIn } from "react-feather";
import { csvToObject } from "@/utils/csv-to-object";
import { CreateSaveExportDto } from "@/dtos/create-save-export.dto";
import { PackageDto } from "@/dtos/package.dto";

type ComponentProps = {
  saves: Save[];
  files: FileMetadataDto[];
  setFiles: Dispatch<SetStateAction<FileMetadataDto[]>>;
  iso: string | undefined;
  setIso: Dispatch<SetStateAction<string | undefined>>;
  setBdExt: Dispatch<SetStateAction<BdExt | undefined>>;
  packages: PackageDto[];
  setPackages: Dispatch<SetStateAction<PackageDto[]>>;
};

const buttonTextImport = "Import save";
const buttonTextLoading = "Importing...";

export default function ImportSave({
  saves,
  files,
  setFiles,
  iso,
  setIso,
  packages,
  setPackages,
  setBdExt,
}: ComponentProps) {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const toggle = () => setModalIsOpen(!modalIsOpen);
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const [saveText, setSaveText] = useState(buttonTextImport);
  const [saveIndex, setSaveIndex] = useState<number>(-1);
  const autoSave = saves.find((save) => save.email !== null);
  const manualSaves = saves.filter((save) => save.email === null);

  const handleImportCsv = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    setModalIsOpen(true);
  };

  const handleImportSave = async (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    setSaveText(buttonTextLoading);
    if (saveIndex === -1) {
      setSaveText(buttonTextImport);
      return;
    }
    const save = saves.find((save) => save.id === saveIndex);
    if (!save) {
      setSaveText(buttonTextImport);
      return;
    }
    const result = await fetchSave(save.id);
    const newFiles = [...files];
    if (!result.fileMetadatas) {
      errorToast("Error while importing data !");
      setSaveText(buttonTextImport);
      return;
    }
    if (result.packages) {
      result.packages = result.packages.sort((a, b) => {
        if (a.index < b.index) {
          return -1;
        }
        if (a.index > b.index) {
          return 1;
        }
        return 0;
      });
      const newPackages = [
        ...result.packages.map((packageDto) => ({
          id: packageDto.uniqueId,
          name: packageDto.name,
          description: packageDto.description,
          goal: packageDto.goal,
          collection: packageDto.collection,
          audience: packageDto.audience,
          language: packageDto.language,
        })),
      ];
      setPackages(newPackages);
    }
    for (const importedFile of result.fileMetadatas) {
      const newFileIndex = newFiles.findIndex(
        (newFile) => newFile.localFile.id === importedFile.localFile.id
      );
      if (newFileIndex === -1) return;
      newFiles[newFileIndex] = {
        ...newFiles[newFileIndex],
        title: importedFile.title,
        description: importedFile.description,
        creator: importedFile.creator,
        publisher: importedFile.publisher,
        dateCreated: importedFile.dateCreated,
        language: importedFile.language,
        license: importedFile.license,
        source: importedFile.source,
        audience: importedFile.audience,
        collection: importedFile.collection,
        collection2: importedFile.collection2,
        size: importedFile.size,
        duration: importedFile.duration,
        subtitles: importedFile.subtitles,
        ignore: importedFile.ignore,
        tag11: importedFile.tag11,
        tag12: importedFile.tag12,
        tag13: importedFile.tag13,
        tag21: importedFile.tag21,
        tag22: importedFile.tag22,
        tag23: importedFile.tag23,
        geography1: importedFile.geography1,
        geography2: importedFile.geography2,
        temporality1: importedFile.temporality1,
        temporality2: importedFile.temporality2,
        package: importedFile.package,
      };
    }
    if (result.iso !== iso) {
      setBdExt(await fetchBdExt(result.iso));
    }
    setFiles(newFiles);
    setIso(result.iso);
    const text =
      save.name !== ""
        ? `Save "${save.name}" imported !`
        : "Automatic save imported !";
    successToast(text);
    setSaveText(buttonTextImport);
    setSaveIndex(-1);
    setButtonDisabled(true);
  };

  const handleSaveChange = (e: ChangeEvent<HTMLSelectElement>) => {
    if (parseInt(e.target.value) === -1 && !buttonDisabled) {
      setButtonDisabled(true);
    } else if (parseInt(e.target.value) !== -1 && buttonDisabled) {
      setButtonDisabled(false);
    }
    setSaveIndex(parseInt(e.target.value));
  };

  const handleFileChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.target.files[0]) {
      setSelectedFile(e.target.files[0]);
    } else {
      setSelectedFile(null);
    }
  };

  const handleConfirm = () => {
    if (selectedFile) {
      const fileReader = new FileReader();
      fileReader.readAsText(selectedFile);
      fileReader.onload = () => {
        const result = fileReader.result as string;
        handleImportFile(result);
      };
      fileReader.onerror = () => {
        errorToast("Error while importing CSV file !");
      };
    } else {
      errorToast("You need to select a file to import !");
    }
  };

  const handleImportFile = async (input: string) => {
    const filesToImport = await csvToObject<CreateSaveExportDto>(input);
    const newFiles = [...files];
    let newIso = undefined;
    for (const fileToImport of filesToImport) {
      const fileIndex = files.findIndex(
        (file) => file.localFile.uniqueId === fileToImport.uniqueId
      );
      if (fileIndex !== -1) {
        newIso = fileToImport.iso;
        const language: any = iso ? {} : { language: newIso };
        const newFile: FileMetadataDto = {
          ...files[fileIndex],
          creator: fileToImport.creator,
          dateCreated: fileToImport.dateCreated,
          description: fileToImport.description,
          license: fileToImport.license,
          publisher: fileToImport.publisher,
          source: fileToImport.source,
          title: fileToImport.title,
          language: fileToImport.language,
          audience: fileToImport.audience,
          size: fileToImport.size,
          duration: fileToImport.duration,
          subtitles: fileToImport.subtitles,
          collection: fileToImport.collection,
          collection2: fileToImport.collection2,
          tag11: fileToImport.tag11,
          tag12: fileToImport.tag12,
          tag13: fileToImport.tag13,
          tag21: fileToImport.tag21,
          tag22: fileToImport.tag22,
          tag23: fileToImport.tag23,
          geography1: fileToImport.geography1,
          geography2: fileToImport.geography2,
          temporality1: fileToImport.temporality1,
          temporality2: fileToImport.temporality2,
          ignore: fileToImport.ignore === "true" ? true : false,
          ...language,
        };
        newFiles[fileIndex] = newFile;
      }
    }
    if (newIso) {
      if (newIso !== iso) {
        setBdExt(await fetchBdExt(newIso));
      }
      setIso(newIso);
    }
    setFiles(newFiles);
    toggle();
    successToast("CSV save has been imported !");
  };

  const modal = (
    <Modal isOpen={modalIsOpen} toggle={toggle}>
      <ModalHeader toggle={toggle}>Please import a save</ModalHeader>
      <ModalBody>
        <form>
          <div className="mb-3">
            <input
              className="form-control"
              type="file"
              id="formFile"
              onChange={handleFileChange}
            />
          </div>
        </form>
      </ModalBody>
      <ModalFooter>
        <Button color="danger" onClick={toggle}>
          Cancel
        </Button>
        <Button color="primary" onClick={handleConfirm}>
          Import
        </Button>
      </ModalFooter>
    </Modal>
  );

  return (
    <>
      <form className="row g-2">
        <div className="col-auto">
          <select
            className="form-select"
            onChange={handleSaveChange}
            value={saveIndex}
            disabled={saves.length === 0}
          >
            {saves.length > 0 ? (
              <option value={-1}>Select a save</option>
            ) : (
              <option value={-1}>No saves available</option>
            )}
            {autoSave && (
              <option value={autoSave.id}>
                Automatic save{" "}
                {` [${moment(parseInt(autoSave.timestamp)).format(
                  "DD/MM/YY HH:mm:ss"
                )}]`}
              </option>
            )}
            {manualSaves.map((save, i) => {
              return (
                <option key={i} value={save.id}>
                  {save.name}
                  {` [${moment(parseInt(save.timestamp)).format(
                    "DD/MM/YY HH:mm:ss"
                  )}]`}
                </option>
              );
            })}
          </select>
        </div>
        <div className="col-auto">
          <button
            onClick={handleImportSave}
            className="btn btn-warning"
            disabled={buttonDisabled}
          >
            <LogIn /> {saveText}
          </button>
        </div>
        <div className="col-auto">
          <button onClick={handleImportCsv} className="btn btn-warning">
            <LogIn /> Import CSV
          </button>
        </div>
      </form>
      {modal}
    </>
  );
}
