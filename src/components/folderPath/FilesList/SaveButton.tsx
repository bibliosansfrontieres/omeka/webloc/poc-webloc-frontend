import { errorToast } from "@/components/toasts/error";
import { successToast } from "@/components/toasts/success";
import { CreateSaveExportDto } from "@/dtos/create-save-export.dto";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { PackageDto } from "@/dtos/package.dto";
import { LocalFolder } from "@/interfaces/local-folder.interface";
import { Save } from "@/interfaces/save.interface";
import { postSave } from "@/lib/post-save";
import { ColumnsSaveTransformation } from "@/utils/columns-save-transformation";
import { downloadCsv } from "@/utils/download-csv";
import { stringify } from "csv-stringify/sync";
import { ChangeEvent, Dispatch, SetStateAction, useState } from "react";
import { Save as SaveIcon } from "react-feather";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";

type ComponentProps = {
  folder: LocalFolder;
  iso: string | undefined;
  files: FileMetadataDto[];
  saves: Save[];
  setSaves: Dispatch<SetStateAction<Save[]>>;
  packages: PackageDto[];
};

export default function SaveButton({
  folder,
  iso,
  files,
  saves,
  setSaves,
  packages,
}: ComponentProps) {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const toggle = () => setModalIsOpen(!modalIsOpen);

  const [name, setName] = useState("");

  const handleNameChange = (e: ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value);
  };

  const handleSave = async () => {
    setModalIsOpen(true);
  };

  const handleConfirm = async () => {
    if (!iso) return;
    if (name === "") {
      errorToast("You need to choose a name for your save !");
      return;
    }
    const save = await postSave(name.trim(), iso, folder, files, packages);
    const newSaves = [save, ...saves];
    setSaves(newSaves);
    setName("");
    setModalIsOpen(false);
    successToast(`All metadata has been saved with the name "${name.trim()}".`);
  };

  const handleToCsv = async () => {
    if (!iso) return;
    if (name === "") {
      errorToast("You need to choose a name for your save !");
      return;
    }

    const output = stringify(
      files.map((file) => new CreateSaveExportDto(file, iso)),
      {
        header: true,
        columns: ColumnsSaveTransformation,
      }
    );

    downloadCsv(output, name);
  };

  const modal = (
    <Modal isOpen={modalIsOpen} toggle={toggle}>
      <ModalHeader toggle={toggle}>Please choose a name</ModalHeader>
      <ModalBody>
        <form>
          <div className="mb-3">
            <label htmlFor="nameInput" className="form-label">
              Name of your save
            </label>
            <input
              type="text"
              className="form-control"
              id="nameInput"
              value={name}
              onChange={handleNameChange}
              placeholder="This is my save"
            />
          </div>
        </form>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={handleToCsv}>
          <SaveIcon /> Export to CSV
        </Button>
        <Button color="success" onClick={handleConfirm}>
          <SaveIcon /> Save
        </Button>
      </ModalFooter>
    </Modal>
  );

  return (
    <>
      <button className="btn btn-success" onClick={handleSave}>
        <SaveIcon /> Save
      </button>
      {modal}
    </>
  );
}
