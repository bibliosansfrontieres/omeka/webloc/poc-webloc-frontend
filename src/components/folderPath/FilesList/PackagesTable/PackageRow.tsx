import { PackageDto } from "@/dtos/package.dto";
import Input from "./Input";
import { Trash } from "react-feather";
import Textarea from "./Textarea";
import Select from "./Select";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { SelectOption } from "@/interfaces/select-option.interface";
import { capitalize } from "@/utils/capitalize";
import MultiSelect from "./MultiSelect";
import { Audience } from "@/interfaces/bd-ext/audience.interface";
import { LanguageList } from "@/interfaces/bd-ext/languages-list.interface";

type ComponentProps = {
  packageDto: PackageDto;
  index: number;
  setPackage: (packageDto: PackageDto, index: number) => void;
  deletePackage: (index: string) => void;
  bdExt: BdExt;
  audiences: Audience[];
  languages: LanguageList;
};

function PackageRow({
  packageDto,
  setPackage,
  deletePackage,
  index,
  bdExt,
  audiences,
  languages,
}: ComponentProps) {
  const collectionOptions: SelectOption[] = bdExt.collections.map(
    (collection) => {
      return { value: collection.name, label: capitalize(collection.name) };
    }
  );

  const audiencesOptions: SelectOption[] = audiences.map((audience) => {
    return { value: audience.name, label: audience.title };
  });

  const languagesOptions: SelectOption[] = languages.special_languages
    .map((language) => {
      return {
        value: language.iso,
        label: `${language.name} (${language.iso})`,
      };
    })
    .concat(
      languages.languages.map((language) => {
        return {
          value: language.iso,
          label: `${language.name} (${language.iso})`,
        };
      })
    );

  const handleSetPackage = (packageDto: PackageDto) => {
    setPackage(packageDto, index);
  };

  return (
    <tr>
      <td style={{ width: "15%" }}>
        <Input
          name="name"
          placeholder="Name"
          packageDto={packageDto}
          setPackage={handleSetPackage}
        />
      </td>
      <td style={{ width: "15%" }}>
        <Textarea
          name="description"
          placeholder="Description"
          packageDto={packageDto}
          setPackage={handleSetPackage}
        />
      </td>
      <td style={{ width: "15%" }}>
        <Textarea
          name="goal"
          placeholder="Goal"
          packageDto={packageDto}
          setPackage={handleSetPackage}
        />
      </td>
      <td style={{ width: "15%" }}>
        <Select
          name="collection"
          placeholder="Collection"
          packageDto={packageDto}
          setPackage={handleSetPackage}
          options={collectionOptions}
        />
      </td>
      <td style={{ width: "15%" }}>
        <MultiSelect
          name="audience"
          placeholder="Select audiences"
          packageDto={packageDto}
          setPackage={handleSetPackage}
          options={audiencesOptions}
        />
      </td>
      <td style={{ width: "15%" }}>
        <MultiSelect
          name="language"
          placeholder="Select languages"
          packageDto={packageDto}
          setPackage={handleSetPackage}
          options={languagesOptions}
        />
      </td>
      <td style={{ width: "15%" }}>
        <button
          className="btn btn-danger btn-sm"
          onClick={() => deletePackage(packageDto.id)}
        >
          <Trash size={"24"} /> Delete
        </button>
      </td>
    </tr>
  );
}

export default PackageRow;
