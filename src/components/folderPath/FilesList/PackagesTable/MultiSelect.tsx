"use client";
import styles from "../FilesTable.module.css";
import { SelectOption } from "@/interfaces/select-option.interface";
import { FocusEvent, memo, useEffect, useState } from "react";
import Select, { MultiValue } from "react-select";
import Validation from "./Validation";
import { PackageDto } from "@/dtos/package.dto";
import {
  ValidationResult,
  isPackageValid,
} from "@/utils/packages-columns-validation";

type ComponentProps = {
  name: string;
  placeholder: string;
  options: SelectOption[];
  packageDto: PackageDto;
  setPackage: (packageDto: PackageDto) => void;
  disabled?: boolean;
};

const defaultClassNames: string[] = [styles.multiSelect];

function MultiSelect({
  name,
  placeholder,
  options,
  packageDto,
  setPackage,
  disabled,
}: ComponentProps) {
  const [stringValue, setStringValue] = useState<string>(packageDto[name]);
  const [value, setValue] = useState<(SelectOption | undefined)[]>([]);
  const [validationResult, setValidationResult] = useState<ValidationResult>({
    valid: true,
  });
  const [classNames, setClassNames] = useState<string[]>([
    ...defaultClassNames,
  ]);

  useEffect(() => {
    const newValidationResult = isPackageValid(name, stringValue, packageDto);
    setValidationResult(newValidationResult);
    if (newValidationResult.valid || disabled) {
      setClassNames([...defaultClassNames]);
    } else {
      setClassNames([...defaultClassNames, "is-invalid"]);
    }
  }, [name, stringValue, packageDto, disabled]);

  useEffect(() => {
    setStringValue(packageDto[name]);
    const selectOptions: SelectOption[] = [];
    const nameSplit = packageDto[name].split(",");
    for (const optionValue of nameSplit) {
      const option = options.find((option) => option.value === optionValue);
      if (option) selectOptions.push(option);
    }
    setValue(selectOptions);
  }, [name, packageDto, options]);

  const handleChange = (newValues: MultiValue<SelectOption | undefined>) => {
    const newOptions: SelectOption[] = [];
    const newStringValues: string[] = [];
    for (const newValue of newValues) {
      if (newValue) {
        newOptions.push(newValue);
        newStringValues.push(newValue.value);
      }
    }
    setValue(newOptions);
    setStringValue(newStringValues.join(","));
  };

  const handleBlur = (e: FocusEvent<HTMLInputElement, Element>) => {
    const newPackage = { ...packageDto };
    newPackage[name] = stringValue;
    setPackage(newPackage);
  };

  return (
    <>
      <Select
        className={classNames.join(" ")}
        styles={{
          container: (base) => {
            if (validationResult.valid || disabled === true) return { ...base };
            return {
              ...base,
              border: "1px solid red",
              borderRadius: "5px",
            };
          },
        }}
        instanceId={`${name}${packageDto.id}`}
        name={name}
        options={options}
        menuShouldScrollIntoView={false}
        placeholder={placeholder}
        onChange={handleChange}
        onBlur={handleBlur}
        value={value}
        isDisabled={disabled}
        isMulti
      />
      {!disabled && <Validation validationResult={validationResult} />}
    </>
  );
}

export default memo(MultiSelect, (props, nextProps) => {
  if (props.packageDto === nextProps.packageDto) return true;
  return false;
});
