"use client";
import { PackageDto } from "@/dtos/package.dto";
import { ValidationResult } from "@/utils/columns-validation";

type ComponentProps = {
  validationResult: ValidationResult;
};

export default function Validation({ validationResult }: ComponentProps) {
  return !validationResult.valid && validationResult.error ? (
    <div className="invalid-feedback">{validationResult.error}</div>
  ) : (
    <></>
  );
}
