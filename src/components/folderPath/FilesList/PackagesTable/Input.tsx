"use client";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { isValid, ValidationResult } from "@/utils/columns-validation";
import {
  ChangeEvent,
  FocusEvent,
  memo,
  useEffect,
  useMemo,
  useState,
} from "react";
import { PackageDto } from "@/dtos/package.dto";
import Validation from "./Validation";
import { isPackageValid } from "@/utils/packages-columns-validation";

type ComponentProps = {
  name: string;
  placeholder: string;
  packageDto: PackageDto;
  setPackage: (packageDto: PackageDto) => void;
  disabled?: boolean;
};
const defaultClassNames = ["form-control"];

function Input({
  name,
  placeholder,
  packageDto,
  setPackage,
  disabled,
}: ComponentProps) {
  const id = Date.now();
  const [value, setValue] = useState<string>(packageDto[name]);
  const [validationResult, setValidationResult] = useState<ValidationResult>({
    valid: true,
  });
  const [classNames, setClassNames] = useState<string[]>([
    ...defaultClassNames,
  ]);

  useEffect(() => {
    const newValidationResult = isPackageValid(name, value, packageDto);
    setValidationResult(newValidationResult);
    if (newValidationResult.valid || disabled) {
      setClassNames([...defaultClassNames]);
    } else {
      setClassNames([...defaultClassNames, "is-invalid"]);
    }
  }, [name, value, packageDto, disabled]);

  useEffect(() => {
    setValue(packageDto[name]);
  }, [name, packageDto]);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  const handleBlur = (e: FocusEvent<HTMLInputElement, Element>) => {
    const newPackage = { ...packageDto };
    newPackage[name] = value;
    setPackage(newPackage);
  };

  return (
    <div className="input-group has-validation">
      <input
        className={classNames.join(" ")}
        type="text"
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={handleChange}
        onBlur={handleBlur}
        disabled={disabled}
        aria-describedby={`test-${name}-${id}`}
      />
      <Validation validationResult={validationResult} />
    </div>
  );
}

export default memo(Input, (props, nextProps) => {
  if (props.packageDto === nextProps.packageDto) return true;
  return false;
});
