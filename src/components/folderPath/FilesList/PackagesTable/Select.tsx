"use client";
import { SelectOption } from "@/interfaces/select-option.interface";
import { ChangeEvent, memo, useEffect, useState } from "react";
import Validation from "./Validation";
import { PackageDto } from "@/dtos/package.dto";
import {
  ValidationResult,
  isPackageValid,
} from "@/utils/packages-columns-validation";

type ComponentProps = {
  name: string;
  placeholder: string;
  options: SelectOption[];
  packageDto: PackageDto;
  setPackage: (packageDto: PackageDto) => void;
  disabled?: boolean;
};

const defaultClassNames = ["form-select"];

function Select({
  name,
  placeholder,
  options,
  packageDto,
  setPackage,
  disabled,
}: ComponentProps) {
  const [validationResult, setValidationResult] = useState<ValidationResult>({
    valid: true,
  });
  const [classNames, setClassNames] = useState<string[]>([
    ...defaultClassNames,
  ]);

  useEffect(() => {
    const newValidationResult = isPackageValid(
      name,
      packageDto[name],
      packageDto
    );
    setValidationResult(newValidationResult);
    if (newValidationResult.valid || disabled) {
      setClassNames([...defaultClassNames]);
    } else {
      setClassNames([...defaultClassNames, "is-invalid"]);
    }
  }, [name, packageDto, disabled]);

  const handleChange = async (e: ChangeEvent<HTMLSelectElement>) => {
    let newPackageDto = { ...packageDto };
    newPackageDto[name] = e.target.value;
    setPackage(newPackageDto);
  };

  return (
    <>
      <select
        className={classNames.join(" ")}
        name={name}
        value={packageDto[name]}
        onChange={handleChange}
        disabled={disabled}
      >
        <option value="">{placeholder}</option>
        {options.map((option, i) => {
          return (
            <option key={i} value={option.value}>
              {option.label}
            </option>
          );
        })}
      </select>
      <Validation validationResult={validationResult} />
    </>
  );
}

export default memo(Select, (props, nextProps) => {
  if (props.packageDto === nextProps.packageDto) return true;
  return false;
});
