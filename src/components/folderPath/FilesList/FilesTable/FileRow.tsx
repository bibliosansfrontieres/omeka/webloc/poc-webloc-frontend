"use client";
import styles from "../FilesTable.module.css";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { Audience } from "@/interfaces/bd-ext/audience.interface";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { License } from "@/interfaces/bd-ext/license.interface";
import { SelectOption } from "@/interfaces/select-option.interface";
import { memo, useEffect } from "react";
import { ExternalLink } from "react-feather";
import Checkbox from "./FileRow/Checkbox";
import Input from "./FileRow/Input";
import MultiSelect from "./FileRow/MultiSelect";
import Select from "./FileRow/Select";
import Textarea from "./FileRow/Textarea";
import { ItemType } from "@/interfaces/item-type.enum";
import Tags from "./FileRow/Tags";
import { isColumnDisabled } from "@/utils/columns-disabling";
import { LanguageList } from "@/interfaces/bd-ext/languages-list.interface";
import { capitalize } from "@/utils/capitalize";
import Temporality from "./FileRow/Temporality";
import Geography from "./FileRow/Geography";
import { PackageDto } from "@/dtos/package.dto";

type ComponentProps = {
  file: FileMetadataDto;
  setFile: (file: FileMetadataDto, index: number) => void;
  index: number;
  bdExt: BdExt;
  languages: LanguageList;
  licenses: License[];
  audiences: Audience[];
  hasSubtitles: boolean;
  hasDuration: boolean;
  packages: PackageDto[];
  fileStatus: { id: number; status: "ready" | "not-ready" } | undefined;
};

function FileRow({
  index,
  file,
  setFile,
  bdExt,
  languages,
  licenses,
  audiences,
  hasSubtitles,
  hasDuration,
  packages,
  fileStatus,
}: ComponentProps) {
  const packageOptions: SelectOption[] = packages.map((packageDto, i) => {
    return { value: packageDto.id, label: `[${i + 1}] ${packageDto.name}` };
  });

  const collectionsOptions: SelectOption[] = bdExt.collections.map(
    (collection) => {
      return { value: collection.name, label: capitalize(collection.name) };
    }
  );

  const languagesOptions: SelectOption[] = languages.special_languages
    .map((language) => {
      return {
        value: language.iso,
        label: `${language.name} (${language.iso})`,
      };
    })
    .concat(
      languages.languages.map((language) => {
        return {
          value: language.iso,
          label: `${language.name} (${language.iso})`,
        };
      })
    );

  const licensesOptions: SelectOption[] = licenses.map((license) => {
    return { value: license.name, label: license.title };
  });

  const audiencesOptions: SelectOption[] = audiences.map((audience) => {
    return { value: audience.name, label: audience.title };
  });

  const handleSetFile = (newFile: FileMetadataDto) => {
    setFile(newFile, index);
  };

  return !fileStatus || fileStatus.status === "ready" ? (
    <tr>
      <td>
        <a
          href={`${process.env.SHAREPOINT_URL}${file.localFile.serverRelativeUrl}`}
          target="_blank"
          className={file.ignore ? "text-muted" : ""}
          rel="noreferrer"
        >
          {file.localFile.name} <ExternalLink />
        </a>
      </td>
      {packages.length > 0 && (
        <td className={styles.tdWidth}>
          <Select
            name="package"
            options={packageOptions}
            placeholder="Select a package"
            file={file}
            setFile={handleSetFile}
          />
        </td>
      )}
      <td className={styles.tdWidth}>
        <Input
          name="title"
          placeholder="Title"
          file={file}
          setFile={handleSetFile}
        />
      </td>
      <td className={styles.tdWidth}>
        <Textarea
          name="description"
          placeholder="Description"
          file={file}
          setFile={handleSetFile}
        />
      </td>
      <td className={styles.tdWidth}>
        <Input
          name="creator"
          placeholder="Creator"
          file={file}
          setFile={handleSetFile}
        />
      </td>
      <td className={styles.tdWidth}>
        <Input
          name="publisher"
          placeholder="Publisher"
          file={file}
          setFile={handleSetFile}
        />
      </td>
      <td className={styles.tdWidth}>
        <Input
          name="dateCreated"
          placeholder="Ex: 2023 / 20?? / 202?"
          file={file}
          setFile={handleSetFile}
        />
      </td>
      <td className={styles.tdWidth}>
        <MultiSelect
          name="language"
          placeholder="Select languages"
          options={languagesOptions}
          file={file}
          setFile={handleSetFile}
        />
      </td>
      <td className={styles.tdWidth}>
        <Select
          name="license"
          placeholder="Select a license"
          options={licensesOptions}
          file={file}
          setFile={handleSetFile}
        />
      </td>
      <td className={styles.tdWidth}>
        <Input
          name="source"
          placeholder="Source"
          file={file}
          setFile={handleSetFile}
        />
      </td>
      <td className={styles.tdWidth}>
        <MultiSelect
          name="audience"
          placeholder="Select audiences"
          options={audiencesOptions}
          file={file}
          setFile={handleSetFile}
        />
      </td>
      <td className={styles.tdWidth}>
        <Input
          indication={file.localFile.type === ItemType.TEXT ? "Pages" : "Mb"}
          name="size"
          placeholder="Size"
          file={file}
          setFile={handleSetFile}
        />
      </td>
      {hasDuration && (
        <td className={styles.tdWidth}>
          <Input
            name="duration"
            placeholder="Duration"
            file={file}
            setFile={handleSetFile}
            disabled={isColumnDisabled("duration", file)}
          />
        </td>
      )}
      {hasSubtitles && (
        <td className={styles.tdWidth}>
          <MultiSelect
            name="subtitles"
            placeholder="Select subtitles"
            options={languagesOptions}
            file={file}
            setFile={handleSetFile}
            disabled={isColumnDisabled("subtitles", file)}
          />
        </td>
      )}
      <td className={styles.tdWidth}>
        <Select
          name="collection"
          placeholder="Select a collection"
          options={collectionsOptions}
          file={file}
          setFile={handleSetFile}
        />
      </td>
      <Tags file={file} setFile={handleSetFile} bdExt={bdExt} index={1} />
      <td className={styles.tdWidth}>
        <Select
          name="collection2"
          placeholder="Select a collection 2"
          options={collectionsOptions}
          file={file}
          setFile={handleSetFile}
        />
      </td>
      <Tags file={file} setFile={handleSetFile} bdExt={bdExt} index={2} />
      <Geography file={file} setFile={handleSetFile} bdExt={bdExt} />
      <Temporality file={file} setFile={handleSetFile} bdExt={bdExt} />
      <td>
        <Checkbox name="ignore" file={file} setFile={handleSetFile} />
      </td>
    </tr>
  ) : file.localFile.compatible ? file.localFile.uploading ? (
    <tr className="text-bg-danger">
      <td>
        <a
          href={`${process.env.SHAREPOINT_URL}${file.localFile.serverRelativeUrl}`}
          target="_blank"
          className={file.ignore ? "text-muted" : ""}
          rel="noreferrer"
        >
          {file.localFile.name} <ExternalLink />
        </a>
      </td>
      <td colSpan={1000} style={{ verticalAlign: "middle" }}>
        Wait for upload end on SharePoint. If upload is finished &gt; the file is corrupted.
      </td>
    </tr>
  ) : (
    <tr className="text-bg-warning">
      <td>
        <a
          href={`${process.env.SHAREPOINT_URL}${file.localFile.serverRelativeUrl}`}
          target="_blank"
          className={file.ignore ? "text-muted" : ""}
          rel="noreferrer"
        >
          {file.localFile.name} <ExternalLink />
        </a>
      </td>
      <td colSpan={1000} style={{ verticalAlign: "middle" }}>
        This file is being downloaded by WebLOC
      </td>
    </tr>
  ) : <></>;
}

export default memo(FileRow, (props, nextProps) => {
  if (
    props.file === nextProps.file &&
    props.packages === nextProps.packages &&
    props.fileStatus === nextProps.fileStatus
  )
    return true;
  return false;
});
