"use client";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { ValidationResult } from "@/utils/columns-validation";

type ComponentProps = {
  file: FileMetadataDto;
  validationResult: ValidationResult;
};

export default function Validation({ file, validationResult }: ComponentProps) {
  return !validationResult.valid && validationResult.error && !file.ignore ? (
    <div className="invalid-feedback">{validationResult.error}</div>
  ) : (
    <></>
  );
}
