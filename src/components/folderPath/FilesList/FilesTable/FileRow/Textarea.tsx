"use client";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { isValid, ValidationResult } from "@/utils/columns-validation";
import { ChangeEvent, FocusEvent, memo, useEffect, useState } from "react";
import Validation from "./Validation";

type ComponentProps = {
  name: string;
  placeholder: string;
  file: FileMetadataDto;
  setFile: (file: FileMetadataDto) => void;
};

const defaultClassNames = ["form-control"];

function Textarea({ name, placeholder, file, setFile }: ComponentProps) {
  const [value, setValue] = useState(file[name]);
  const [validationResult, setValidationResult] = useState<ValidationResult>({
    valid: true,
  });
  const [classNames, setClassNames] = useState<string[]>([
    ...defaultClassNames,
  ]);

  useEffect(() => {
    const newValidationResult = isValid(name, value, file);
    setValidationResult(newValidationResult);
    if (newValidationResult.valid || file.ignore) {
      setClassNames([...defaultClassNames]);
    } else {
      setClassNames([...defaultClassNames, "is-invalid"]);
    }
  }, [name, value, file]);

  useEffect(() => {
    setValue(file[name]);
  }, [name, file]);

  const handleChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setValue(e.target.value);
  };

  const handleBlur = (e: FocusEvent<HTMLTextAreaElement, Element>) => {
    const newFile = { ...file };
    newFile[name] = value;
    setFile(newFile);
  };

  return (
    <>
      <textarea
        className={classNames.join(" ")}
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={handleChange}
        onBlur={handleBlur}
        disabled={file.ignore}
      />
      <Validation file={file} validationResult={validationResult} />
    </>
  );
}

export default memo(Textarea, (props, nextProps) => {
  if (props.file === nextProps.file) return true;
  return false;
});
