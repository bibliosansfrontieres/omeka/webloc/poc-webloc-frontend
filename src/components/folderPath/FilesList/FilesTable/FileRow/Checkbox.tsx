"use client";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { ChangeEvent, memo } from "react";

type ComponentProps = {
  name: string;
  file: FileMetadataDto;
  setFile: (file: FileMetadataDto) => void;
};

function Checkbox({ name, file, setFile }: ComponentProps) {
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const newFile = { ...file };
    newFile[name] = !file[name];
    setFile(newFile);
  };

  return (
    <input
      className="form-check-input"
      type="checkbox"
      name={name}
      checked={file[name]}
      onChange={handleChange}
    />
  );
}

export default memo(Checkbox, (props, nextProps) => {
  if (props.file === nextProps.file) return true;
  return false;
});
