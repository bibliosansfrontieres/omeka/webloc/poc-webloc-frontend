"use client";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { SelectOption } from "@/interfaces/select-option.interface";
import { isValid, ValidationResult } from "@/utils/columns-validation";
import { ChangeEvent, memo, useCallback, useEffect, useState } from "react";
import Validation from "./Validation";

type ComponentProps = {
  name: string;
  placeholder: string;
  options: SelectOption[];
  file: FileMetadataDto;
  setFile: (file: FileMetadataDto) => void;
  onChange?: (
    newFile: FileMetadataDto,
    name: string
  ) => FileMetadataDto | Promise<FileMetadataDto>;
  disabled?: boolean;
};

const defaultClassNames = ["form-select"];

function Select({
  name,
  placeholder,
  options,
  file,
  setFile,
  disabled,
  onChange,
}: ComponentProps) {
  const [validationResult, setValidationResult] = useState<ValidationResult>({
    valid: true,
  });
  const [classNames, setClassNames] = useState<string[]>([
    ...defaultClassNames,
  ]);

  useEffect(() => {
    const newValidationResult = isValid(name, file[name], file);
    setValidationResult(newValidationResult);
    if (newValidationResult.valid || file.ignore || disabled) {
      setClassNames([...defaultClassNames]);
    } else {
      setClassNames([...defaultClassNames, "is-invalid"]);
    }
  }, [name, file, disabled]);

  const handleChange = async (e: ChangeEvent<HTMLSelectElement>) => {
    // TODO do something for collection, its doesnt have to be here
    let newFile = { ...file };
    newFile[name] = e.target.value;
    if (name === "temporality1") {
      newFile["temporality2"] = "";
    }
    if (name === "geography1") {
      newFile["geography2"] = "";
    }
    if (name === "collection") {
      newFile["tag1"] = "";
      newFile["tag2"] = "";
      newFile["tag3"] = "";
    }
    if (name === "tag1") {
      newFile["tag2"] = "";
      newFile["tag3"] = "";
    }
    if (name === "tag2") {
      newFile["tag3"] = "";
    }
    if (onChange) {
      newFile = await onChange(newFile, name);
    }
    setFile(newFile);
  };

  return (
    <>
      <select
        className={classNames.join(" ")}
        name={name}
        value={file[name]}
        onChange={handleChange}
        disabled={file.ignore || disabled}
      >
        <option value="">{placeholder}</option>
        {options.map((option, i) => {
          return (
            <option key={i} value={option.value}>
              {option.label}
            </option>
          );
        })}
      </select>
      <Validation file={file} validationResult={validationResult} />
    </>
  );
}

export default memo(Select, (props, nextProps) => {
  if (
    props.file === nextProps.file &&
    props.options === nextProps.options &&
    props.disabled === nextProps.disabled
  )
    return true;
  return false;
});
