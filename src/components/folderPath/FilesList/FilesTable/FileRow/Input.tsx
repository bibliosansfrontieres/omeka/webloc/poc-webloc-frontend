"use client";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { isValid, ValidationResult } from "@/utils/columns-validation";
import {
  ChangeEvent,
  FocusEvent,
  memo,
  useEffect,
  useMemo,
  useState,
} from "react";
import Validation from "./Validation";

type ComponentProps = {
  name: string;
  placeholder: string;
  indication?: string;
  file: FileMetadataDto;
  setFile: (file: FileMetadataDto) => void;
  disabled?: boolean;
};

function Input({
  name,
  placeholder,
  indication,
  file,
  setFile,
  disabled,
}: ComponentProps) {
  const defaultClassNames = useMemo(() => {
    const def = ["form-control"];
    if (indication) def.push("remove-border-right");
    return def;
  }, [indication]);
  const [value, setValue] = useState<string>(file[name]);
  const [validationResult, setValidationResult] = useState<ValidationResult>({
    valid: true,
  });
  const [classNames, setClassNames] = useState<string[]>([
    ...defaultClassNames,
  ]);

  useEffect(() => {
    const newValidationResult = isValid(name, value, file);
    setValidationResult(newValidationResult);
    if (newValidationResult.valid || disabled || file.ignore) {
      setClassNames([...defaultClassNames]);
    } else {
      setClassNames([...defaultClassNames, "is-invalid"]);
    }
  }, [name, value, file, defaultClassNames, disabled]);

  useEffect(() => {
    setValue(file[name]);
  }, [name, file]);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  const handleBlur = (e: FocusEvent<HTMLInputElement, Element>) => {
    const newFile = { ...file };
    newFile[name] = value;
    setFile(newFile);
  };

  return (
    <div className="input-group has-validation">
      <input
        className={classNames.join(" ")}
        type="text"
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={handleChange}
        onBlur={handleBlur}
        disabled={file.ignore || disabled}
        aria-describedby={`test-${name}-${file.localFile.id}`}
      />
      {indication && (
        <span
          className="input-group-text remove-border-left"
          id={`test-${name}-${file.localFile.id}`}
        >
          {indication}
        </span>
      )}
      {!disabled && (
        <Validation file={file} validationResult={validationResult} />
      )}
    </div>
  );
}

export default memo(Input, (props, nextProps) => {
  if (props.file === nextProps.file) return true;
  return false;
});
