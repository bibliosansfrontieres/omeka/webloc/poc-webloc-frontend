import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import {
  BdExt,
  BdExtLevel1,
  BdExtLevel2,
} from "@/interfaces/bd-ext/bd-ext.interface";
import { SelectOption } from "@/interfaces/select-option.interface";
import { useEffect, useState } from "react";
import styles from "../../FilesTable.module.css";
import Select from "./Select";
import { capitalize } from "@/utils/capitalize";

type ComponentProps = {
  file: FileMetadataDto;
  setFile: (file: FileMetadataDto) => void;
  bdExt: BdExt;
};

export default function Geography({ file, setFile, bdExt }: ComponentProps) {
  const options1: SelectOption[] = bdExt.locations.map((level1) => {
    return { value: level1.name, label: capitalize(level1.name) };
  });
  const [options2, setOptions2] = useState<SelectOption[]>([]);

  useEffect(() => {
    if (file.geography1 !== "") {
      const collection = bdExt.locations.find(
        (date) => date.name === file.geography1
      );
      console.log(collection);
      if (collection && collection.level2) {
        const newOptions = collection.level2.map((lvl2) => ({
          value: lvl2.name,
          label: capitalize(lvl2.name),
        }));
        setOptions2(newOptions);
      } else {
        setOptions2([]);
      }
    } else {
      setOptions2([]);
    }
  }, [file.geography1, bdExt.locations]);

  return (
    <>
      <td className={styles.tdWidth}>
        <Select
          name={`geography1`}
          placeholder={`Select geography 1`}
          options={options1}
          file={file}
          setFile={setFile}
        />
      </td>
      <td className={styles.tdWidth}>
        <Select
          name={`geography2`}
          placeholder={`Select geography 2`}
          options={options2}
          file={file}
          setFile={setFile}
          disabled={options2.length === 0}
        />
      </td>
    </>
  );
}
