import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import {
  BdExt,
  BdExtLevel1,
  BdExtLevel2,
} from "@/interfaces/bd-ext/bd-ext.interface";
import { SelectOption } from "@/interfaces/select-option.interface";
import { useEffect, useState } from "react";
import styles from "../../FilesTable.module.css";
import Select from "./Select";
import { capitalize } from "@/utils/capitalize";

type ComponentProps = {
  file: FileMetadataDto;
  setFile: (file: FileMetadataDto) => void;
  bdExt: BdExt;
  index: number;
};

export default function Tags({ file, setFile, bdExt, index }: ComponentProps) {
  const [collection, setCollection] = useState<BdExtLevel1 | undefined>(
    undefined
  );
  const [level2, setLevel2] = useState<BdExtLevel2 | undefined>(undefined);
  const [options1, setOptions1] = useState<SelectOption[]>([]);
  const [options2, setOptions2] = useState<SelectOption[]>([]);
  const [options3, setOptions3] = useState<SelectOption[]>([]);

  const collect = index === 1 ? file.collection : file.collection2;
  const tag1 = index === 1 ? file.tag11 : file.tag21;
  const tag2 = index === 1 ? file.tag12 : file.tag22;
  const tag3 = index === 1 ? file.tag13 : file.tag23;

  useEffect(() => {
    if (collect !== "") {
      const levels2 = bdExt.collections.find(
        (collection) => collection.name === collect
      );
      if (levels2 && levels2.level2) {
        const newOptions = levels2.level2.map((level2) => ({
          value: level2.name,
          label: capitalize(level2.name),
        }));
        setOptions1(newOptions);
        setCollection(levels2);
      } else {
        setCollection(undefined);
        setOptions1([]);
      }
    } else {
      setOptions1([]);
    }
  }, [collect, bdExt.collections]);

  useEffect(() => {
    if (tag1 !== "" && collection && collection.level2) {
      const levels3 = collection.level2.find((level2) => level2.name === tag1);
      if (levels3 && levels3.level3) {
        const newOptions = levels3.level3.map((level3) => ({
          value: level3.name,
          label: capitalize(level3.name),
        }));
        setOptions2(newOptions);
        setLevel2(levels3);
      } else {
        setLevel2(undefined);
        setOptions2([]);
      }
    } else {
      setOptions2([]);
    }
  }, [tag1, collection]);

  useEffect(() => {
    if (tag2 !== "" && level2 && level2.level3) {
      const levels4 = level2.level3.find((level3) => level3.name === tag2);
      if (levels4 && levels4.level4) {
        const newOptions = levels4.level4.map((level4) => ({
          value: level4.name,
          label: capitalize(level4.name),
        }));
        setOptions3(newOptions);
      } else {
        setOptions3([]);
      }
    } else {
      setOptions3([]);
    }
  }, [tag2, level2]);

  return (
    <>
      <td className={styles.tdWidth}>
        <Select
          name={`tag${index}1`}
          placeholder={`Select tag ${index}-1`}
          options={options1}
          file={file}
          setFile={setFile}
          disabled={options1.length === 0}
        />
      </td>
      <td className={styles.tdWidth}>
        <Select
          name={`tag${index}2`}
          placeholder={`Select tag ${index}-2`}
          options={options2}
          file={file}
          setFile={setFile}
          disabled={options2.length === 0}
        />
      </td>
      <td className={styles.tdWidth}>
        <Select
          name={`tag${index}3`}
          placeholder={`Select tag ${index}-3`}
          options={options3}
          file={file}
          setFile={setFile}
          disabled={options3.length === 0}
        />
      </td>
    </>
  );
}
