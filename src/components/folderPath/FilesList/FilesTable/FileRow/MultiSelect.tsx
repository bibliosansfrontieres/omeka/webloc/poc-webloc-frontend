"use client";
import styles from "../../FilesTable.module.css";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { SelectOption } from "@/interfaces/select-option.interface";
import { isValid, ValidationResult } from "@/utils/columns-validation";
import { FocusEvent, memo, useEffect, useState } from "react";
import Select, { MultiValue } from "react-select";
import Validation from "./Validation";

type ComponentProps = {
  name: string;
  placeholder: string;
  options: SelectOption[];
  file: FileMetadataDto;
  setFile: (file: FileMetadataDto) => void;
  disabled?: boolean;
};

const defaultClassNames: string[] = [styles.multiSelect];

function MultiSelect({
  name,
  placeholder,
  options,
  file,
  setFile,
  disabled,
}: ComponentProps) {
  const [stringValue, setStringValue] = useState<string>(file[name]);
  const [value, setValue] = useState<(SelectOption | undefined)[]>([]);
  const [validationResult, setValidationResult] = useState<ValidationResult>({
    valid: true,
  });
  const [classNames, setClassNames] = useState<string[]>([
    ...defaultClassNames,
  ]);

  useEffect(() => {
    const newValidationResult = isValid(name, stringValue, file);
    setValidationResult(newValidationResult);
    if (newValidationResult.valid || file.ignore || disabled) {
      setClassNames([...defaultClassNames]);
    } else {
      setClassNames([...defaultClassNames, "is-invalid"]);
    }
  }, [name, stringValue, file, disabled]);

  useEffect(() => {
    setStringValue(file[name]);
    const selectOptions: SelectOption[] = [];
    const nameSplit = file[name].split(",");
    for (const optionValue of nameSplit) {
      const option = options.find((option) => option.value === optionValue);
      if (option) selectOptions.push(option);
    }
    setValue(selectOptions);
  }, [name, file, options]);

  const handleChange = (newValues: MultiValue<SelectOption | undefined>) => {
    const newOptions: SelectOption[] = [];
    const newStringValues: string[] = [];
    for (const newValue of newValues) {
      if (newValue) {
        newOptions.push(newValue);
        newStringValues.push(newValue.value);
      }
    }
    setValue(newOptions);
    setStringValue(newStringValues.join(","));
    console.log(newOptions);
  };

  const handleBlur = (e: FocusEvent<HTMLInputElement, Element>) => {
    const newFile = { ...file };
    newFile[name] = stringValue;
    console.log(stringValue, "string");
    setFile(newFile);
  };

  return (
    <>
      <Select
        className={classNames.join(" ")}
        styles={{
          container: (base) => {
            if (validationResult.valid || file.ignore || disabled === true)
              return { ...base };
            return {
              ...base,
              border: "1px solid red",
              borderRadius: "5px",
            };
          },
        }}
        instanceId={`${name}${file.localFile.id}`}
        name={name}
        options={options}
        menuShouldScrollIntoView={false}
        placeholder={placeholder}
        onChange={handleChange}
        onBlur={handleBlur}
        value={value}
        isDisabled={file.ignore || disabled}
        isMulti
      />
      {!disabled && (
        <Validation file={file} validationResult={validationResult} />
      )}
    </>
  );
}

export default memo(MultiSelect, (props, nextProps) => {
  if (props.file === nextProps.file) return true;
  return false;
});
