"use client";
import styles from "./FilesTable.module.css";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { Audience } from "@/interfaces/bd-ext/audience.interface";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { Language } from "@/interfaces/bd-ext/language.interface";
import { License } from "@/interfaces/bd-ext/license.interface";
import FileRow from "./FilesTable/FileRow";
import { ItemType } from "@/interfaces/item-type.enum";
import { LanguageList } from "@/interfaces/bd-ext/languages-list.interface";
import { PackageDto } from "@/dtos/package.dto";
import { memo } from "react";
import { FolderStatus } from "@/interfaces/folder-status.interface";

type ComponentProps = {
  files: FileMetadataDto[];
  setFile: (file: FileMetadataDto, index: number) => void;
  bdExt: BdExt;
  languages: LanguageList;
  licenses: License[];
  audiences: Audience[];
  packages: PackageDto[];
  folderStatus: FolderStatus;
};

function FilesTable({
  files,
  setFile,
  bdExt,
  languages,
  licenses,
  audiences,
  packages,
  folderStatus,
}: ComponentProps) {
  const hasSubtitles =
    files.find((file) => file.localFile.type === ItemType.MOVING_IMAGE) !==
    undefined;

  const hasDuration =
    hasSubtitles ||
    files.find(
      (file) =>
        file.localFile.type === ItemType.SOUND ||
        file.localFile.type === ItemType.MOVING_IMAGE
    ) !== undefined;

  return (
    <>
      <h3 className="my-3">Files metadata</h3>
      <div className={["mb-5", styles.scroll].join(" ")}>
        <table className={["table", styles.filesTable].join(" ")}>
          <thead>
            <tr>
              <th>File ({files.length})</th>
              {packages.length > 0 && <th>Package</th>}
              <th>Title</th>
              <th>Description</th>
              <th>Creator</th>
              <th>Publisher</th>
              <th>Date Created</th>
              <th>Language</th>
              <th>License</th>
              <th>Source</th>
              <th>Audience</th>
              <th>Size</th>
              {hasDuration && <th>Duration</th>}
              {hasSubtitles && <th>Subtitles</th>}
              <th>Collection</th>
              <th>Tag 1</th>
              <th>Tag 2</th>
              <th>Tag 3</th>
              <th>Collection 2</th>
              <th>Tag 2-1</th>
              <th>Tag 2-2</th>
              <th>Tag 2-3</th>
              <th>Geography 1</th>
              <th>Geography 2</th>
              <th>Temporality 1</th>
              <th>Temporality 2</th>
              <th>Ignore</th>
            </tr>
          </thead>
          <tbody>
            {files.map((file, i) => {
              const fileStatus = folderStatus.localFiles.find(
                (localFile) => localFile.id === file.localFile.id
              );
              return (
                <FileRow
                  key={i}
                  file={file}
                  index={i}
                  setFile={setFile}
                  bdExt={bdExt}
                  fileStatus={fileStatus}
                  languages={languages}
                  licenses={licenses}
                  audiences={audiences}
                  hasSubtitles={hasSubtitles}
                  hasDuration={hasDuration}
                  packages={packages}
                />
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default memo(FilesTable, (props, nextProps) => {
  if (
    props.files === nextProps.files &&
    props.packages === nextProps.packages &&
    props.folderStatus === nextProps.folderStatus
  )
    return true;
  return false;
});
