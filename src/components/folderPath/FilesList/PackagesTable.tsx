import { PackageDto } from "@/dtos/package.dto";
import { MouseEvent, memo, useEffect, useState } from "react";
import { Plus } from "react-feather";
import PackageRow from "./PackagesTable/PackageRow";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { Audience } from "@/interfaces/bd-ext/audience.interface";
import { LanguageList } from "@/interfaces/bd-ext/languages-list.interface";
import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";

type ComponentProps = {
  packages: PackageDto[];
  setPackage: (packageDto: PackageDto, index: number) => void;
  addPackage: (iso: string) => void;
  deletePackage: (id: string) => void;
  iso: string;
  files: FileMetadataDto[];
  bdExt: BdExt;
  audiences: Audience[];
  languages: LanguageList;
};

function PackagesTable({
  packages,
  files,
  addPackage,
  setPackage,
  deletePackage,
  iso,
  bdExt,
  audiences,
  languages,
}: ComponentProps) {
  const [modalId, setModalId] = useState<string | undefined>(undefined);

  const handleAddPackage = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    addPackage(iso);
  };

  const handleDeletePackage = (id: string) => {
    let isUsedPackage = false;
    for (const file of files) {
      if (file.package === id) {
        isUsedPackage = true;
        break;
      }
    }
    if (isUsedPackage) {
      setModalId(id);
      return;
    }
    deletePackage(id);
  };

  const closeModal = () => setModalId(undefined);

  const deletePackageModal = (
    <Modal isOpen={modalId !== undefined} toggle={closeModal}>
      <ModalHeader toggle={closeModal}>Warning !</ModalHeader>
      <ModalBody>
        You want to delete package &quot;
        {packages.find((pck, i) => pck.id === modalId || "")?.name ||
          "Undefined"}
        &quot;.
        <br />
        But this package is used in some items.
        <br />
        Are you sure you want to delete this package ?
      </ModalBody>
      <ModalFooter>
        <Button
          color="success"
          onClick={() => {
            deletePackage(modalId || "");
            closeModal();
          }}
        >
          Confirm
        </Button>
        <Button color="danger" onClick={() => closeModal()}>
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );

  return (
    <>
      {deletePackageModal}
      <div className="mb-5">
        <div className="row g-2">
          <div className="col-auto" style={{ marginRight: "1em" }}>
            <h3>Packages metadata</h3>
          </div>
          <div className="col-auto">
            <button className="btn btn-success" onClick={handleAddPackage}>
              <Plus /> Add package
            </button>
          </div>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Goal</th>
              <th>Collection</th>
              <th>Audience</th>
              <th>Language</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {packages.length > 0 ? (
              packages.map((packageDto, i) => (
                <PackageRow
                  key={i}
                  index={i}
                  packageDto={packageDto}
                  setPackage={setPackage}
                  deletePackage={handleDeletePackage}
                  bdExt={bdExt}
                  audiences={audiences}
                  languages={languages}
                />
              ))
            ) : (
              <tr>
                <td colSpan={1000}>No packages created</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default PackagesTable;
