"use client";

import { signIn } from "next-auth/react";
import Image from "next/image";

export default function SignInForm() {
  const handleSignin = async (e: any) => {
    e.preventDefault();
    const result = await signIn("azure-ad", { callbackUrl: "/" });
  };

  return (
    <form>
      <Image
        src="/BSF_Logo.png"
        alt="BSF logo"
        width={235}
        height={72}
        className="logo"
      />
      <div>
        <Image
          src="/microsoft365.png"
          height="53"
          width="48"
          alt="Logo Microsoft 365"
          style={{ marginRight: "1em" }}
        />
        <button className="btn btn-lg btn-primary" onClick={handleSignin}>
          <span>Sign in with Microsoft 365</span>
        </button>
      </div>
      <p className="text-muted bottom">&copy; {new Date().getFullYear()}</p>
    </form>
  );
}
