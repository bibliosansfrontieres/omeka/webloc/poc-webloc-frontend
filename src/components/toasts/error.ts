import { toast } from "react-toastify";

export function errorToast(text: string, autoClose?: number) {
  autoClose = autoClose ? autoClose : 5000;
  return toast.error(text, {
    position: "top-center",
    autoClose,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "light",
  });
}
