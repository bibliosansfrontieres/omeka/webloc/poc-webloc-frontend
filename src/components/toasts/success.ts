import { toast } from "react-toastify";

export function successToast(text: string, autoClose?: number) {
  autoClose = autoClose ? autoClose : 5000;
  toast.success(text, {
    position: "top-center",
    autoClose,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "light",
  });
}
