import { OmekaExport } from "@/interfaces/omeka-export.interface";
import { Save } from "react-feather";
import moment from "moment";

type ComponentProps = {
  omekaExport: OmekaExport;
};

export default function OmekaExportRow({ omekaExport }: ComponentProps) {
  const handleDownload = () => {
    window.open(
      `${process.env.PUBLIC_API_URL}/omeka-export/download/${omekaExport.id}`
    );
  };
  return (
    <tr>
      <td>{omekaExport.id}</td>
      <td>{moment(omekaExport.timestamp).format("MM/DD/YYYY HH:mm:ss")}</td>
      <td>{omekaExport.email}</td>
      <td>{omekaExport.packages}</td>
      <td>{omekaExport.ready ? "Ready to download" : "Exporting..."}</td>
      <td>
        <button
          className="btn btn-sm btn-primary"
          disabled={!omekaExport.ready}
          onClick={handleDownload}
        >
          <Save />
        </button>
      </td>
    </tr>
  );
}
