import Link from "next/link";

type ComponentProps = {
  pages: number;
  page: number;
};

export default function Pagination({ pages, page }: ComponentProps) {
  return (
    <nav aria-label="Page navigation example">
      <ul className="pagination justify-content-center">
        <li className={["page-item", page === 1 ? "disabled" : ""].join(" ")}>
          <Link
            className="page-link"
            href={`/omeka-export/download?page=${page - 1}`}
          >
            <span aria-hidden="true">&laquo;</span>
          </Link>
        </li>
        {page > 3 && (
          <li className="page-item">
            <Link className="page-link" href="/omeka-export/download?page=1">
              1
            </Link>
          </li>
        )}
        {page > 4 && (
          <li className="page-item disabled">
            <a className="page-link" href="#">
              ...
            </a>
          </li>
        )}
        {page - 2 > 0 && (
          <li className="page-item">
            <Link
              className="page-link"
              href={`/omeka-export/download?page=${page - 2}`}
            >
              {page - 2}
            </Link>
          </li>
        )}
        {page - 1 > 0 && (
          <li className="page-item">
            <Link
              className="page-link"
              href={`/omeka-export/download?page=${page - 1}`}
            >
              {page - 1}
            </Link>
          </li>
        )}
        <li className="page-item active">
          <Link
            className="page-link"
            href={`/omeka-export/download?page=${page}`}
          >
            {page}
          </Link>
        </li>
        {page + 1 <= pages && (
          <li className="page-item">
            <Link
              className="page-link"
              href={`/omeka-export/download?page=${page + 1}`}
            >
              {page + 1}
            </Link>
          </li>
        )}
        {page + 2 <= pages && (
          <li className="page-item">
            <Link
              className="page-link"
              href={`/omeka-export/download?page=${page + 2}`}
            >
              {page + 2}
            </Link>
          </li>
        )}
        {page < pages - 3 && (
          <li className="page-item disabled">
            <a className="page-link" href="#">
              ...
            </a>
          </li>
        )}
        {page < pages - 2 && (
          <li className="page-item">
            <Link
              className="page-link"
              href={`/omeka-export/download?page=${pages}`}
            >
              {pages}
            </Link>
          </li>
        )}
        <li
          className={["page-item", page >= pages ? "disabled" : ""].join(" ")}
        >
          <Link
            className="page-link"
            href={`/omeka-export/download?page=${page + 1}`}
          >
            <span aria-hidden="true">&raquo;</span>
          </Link>
        </li>
      </ul>
    </nav>
  );
}
