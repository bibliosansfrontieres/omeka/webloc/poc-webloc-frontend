"use client";
import { OmekaExport } from "@/interfaces/omeka-export.interface";
import { fetchOmekaExports } from "@/lib/fetch-omeka-exports";
import { useEffect, useState } from "react";
import { useSearchParams } from "next/navigation";
import Menu from "./Menu";
import OmekaExportRow from "./download/OmekaExportRow";
import Pagination from "./download/Pagination";

export default function OmekaExportDownload() {
  const [exports, setExports] = useState<OmekaExport[]>([]);
  const [pages, setPages] = useState<number>(0);

  const searchParams = useSearchParams();
  const page = parseInt(searchParams?.get("page") || "1") || 1;

  useEffect(() => {
    let interval: NodeJS.Timer | undefined = undefined;
    let foundNotReadyExport = false;
    fetchOmekaExports(page).then((result) => {
      foundNotReadyExport = !!result.exports.find((oExport) => !oExport.ready);
      setExports(result.exports);
      setPages(result.pages);
      if (foundNotReadyExport) {
        interval = setInterval(() => {
          console.log(`INTERVAL ${Date.now()}`);
          let foundNotReadyExport = false;
          fetchOmekaExports(page).then((result) => {
            foundNotReadyExport = !!result.exports.find(
              (oExport) => !oExport.ready
            );
            setExports(result.exports);
            setPages(result.pages);
            if (!foundNotReadyExport) {
              clearInterval(interval);
            }
          });
        }, 5000);
      }
    });
    return () => {
      if (interval !== undefined) {
        clearInterval(interval);
      }
    };
  }, [page]);

  return (
    <>
      <Menu />
      <Pagination pages={pages} page={page} />
      <table className="table">
        <thead>
          <tr>
            <td>ID</td>
            <td>Date</td>
            <td>User</td>
            <td>Packages</td>
            <td>Status</td>
            <td>Download</td>
          </tr>
        </thead>
        <tbody>
          {exports.map((omekaExport, i) => {
            return <OmekaExportRow key={i} omekaExport={omekaExport} />;
          })}
        </tbody>
      </table>
      <Pagination pages={pages} page={page} />
    </>
  );
}
