"use client";
import { ChangeEvent, useState } from "react";
import { errorToast } from "../toasts/error";
import { csvToObject } from "@/utils/csv-to-object";
import { successToast } from "../toasts/success";
import { useRouter } from "next/navigation";
import { postOmekaExport } from "@/lib/post-omeka-export";
import { useSession } from "next-auth/react";
import Menu from "./Menu";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { postOmekaExportAll } from "@/lib/post-omeka-export-all";

export default function OmekaExport() {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const modalToggle = () => setModalOpen(!modalOpen);

  const router = useRouter();
  const session = useSession();

  const handleFileChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.target.files[0]) {
      setSelectedFile(e.target.files[0]);
    } else {
      setSelectedFile(null);
    }
  };

  const handleConfirm = () => {
    if (selectedFile) {
      const fileReader = new FileReader();
      fileReader.readAsText(selectedFile);
      fileReader.onload = () => {
        const result = fileReader.result as string;
        handleImportFile(result);
      };
      fileReader.onerror = () => {
        errorToast("Error while importing CSV file !");
      };
    } else {
      errorToast("You need to select a file to import !");
    }
  };

  const handleConfirmAll = () => modalToggle();

  const handleConfirmAllConfirm = async () => {
    modalToggle();
    await postOmekaExportAll(session.data);
    successToast("Omeka export successfully asked !");
    router.push("/omeka-export/download");
  };

  const handleImportFile = async (input: string) => {
    const packagesToImport = await csvToObject<any>(input);
    const newPackagesToImport = packagesToImport.filter(
      (pck) => pck.package_id !== undefined
    );
    const newProjectsToImport = packagesToImport.filter(
      (pck) => pck.project_id !== undefined
    );
    await postOmekaExport(
      newPackagesToImport,
      newProjectsToImport,
      session.data
    );
    successToast("Omeka export successfully asked !");
    router.push("/omeka-export/download");
  };

  const modal = (
    <Modal isOpen={modalOpen} toggle={modalToggle}>
      <ModalHeader toggle={modalToggle}>Warning !</ModalHeader>
      <ModalBody>
        Your are exporting all Omeka packages.
        <br />
        This operation is long and ask a lot of ressources of Omeka database.
        <br />
        Please do this operation when you really need it.
      </ModalBody>
      <ModalFooter>
        <Button color="warning" onClick={handleConfirmAllConfirm}>
          Export all Omeka packages
        </Button>
        <Button color="danger" onClick={modalToggle}>
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );

  return (
    <>
      {modal}
      <Menu />
      <p>
        You need to create a CSV file, with a column &apos;package_id&apos; or
        &apos;project_id&apos;.
        <br />
        In this column, you can put all ids of the packages you want to export.
      </p>
      <p>
        You can optionnaly add, as much as you want, other columns that will be
        transfered to the exported data.
      </p>
      <p>
        <a href="/example.csv">Download example CSV file</a>
      </p>
      <div className="mb-3">
        <input
          className="form-control"
          type="file"
          id="formFile"
          onChange={handleFileChange}
        />
      </div>
      <div className="d-grid">
        <button className="btn btn-primary" onClick={handleConfirm}>
          Ask Omeka packages export
        </button>
        <button className="btn btn-warning mt-5" onClick={handleConfirmAll}>
          Ask Omeka all packages export
        </button>
      </div>
    </>
  );
}
