"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";

export default function Menu() {
  const pathname = usePathname();
  return (
    <ul className="nav nav-tabs mb-5">
      <li className="nav-item">
        <Link
          href="/omeka-export"
          className={[
            "nav-link",
            pathname === "/omeka-export" ? "active" : "",
          ].join(" ")}
        >
          Ask Omeka export
        </Link>
      </li>
      <li className="nav-item">
        <Link
          href="/omeka-export/download"
          className={[
            "nav-link",
            pathname === "/omeka-export/download" ? "active" : "",
          ].join(" ")}
        >
          Download asked Omeka export
        </Link>
      </li>
    </ul>
  );
}
