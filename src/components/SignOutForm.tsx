"use client";

import { signOut } from "next-auth/react";
import Image from "next/image";
import { useRouter } from "next/navigation";

export default function SignOutForm() {
  const router = useRouter();
  const handleSignoutClick = () => {
    signOut();
    router.push("/signin");
  };
  const handleCancelClick = () => {
    router.push("/");
  };
  return (
    <>
      <Image
        src="/BSF_Logo.png"
        alt="BSF logo"
        width={235}
        height={72}
        className="logo"
      />
      <p style={{ marginBottom: "3em" }} className="fs-3">
        Are your sure you want to sign out ?
      </p>
      <div style={{ display: "flex", justifyContent: "space-around" }}>
        <button className="btn btn-lg btn-success" onClick={handleSignoutClick}>
          <span>Sign out</span>
        </button>
        <button className="btn btn-lg btn-danger" onClick={handleCancelClick}>
          <span>Cancel</span>
        </button>
      </div>
      <p className="text-muted bottom">&copy; {new Date().getFullYear()}</p>
    </>
  );
}
