import { FileFormat } from "./file-format.enum";
import { ItemType } from "./item-type.enum";
import { LocalFolder } from "./local-folder.interface";

export interface LocalFile {
  id: number;
  changeDetected: boolean;
  ready: boolean;
  localFolder?: LocalFolder;
  uniqueId: string;
  timeLastModified: string;
  timeCreated: string;
  uploading: boolean;
  compatible: boolean;
  serverRelativeUrl: string;
  extension: string;
  format: FileFormat;
  type: ItemType;
  size: string | null;
  duration: string | null;
  subtitles: number | null;
  audios: number | null;
  [key: string]: any;
}
