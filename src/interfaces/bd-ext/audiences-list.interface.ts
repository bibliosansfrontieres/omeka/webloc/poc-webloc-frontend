import { Audience } from "./audience.interface";

export interface AudiencesList {
  iso: string;
  audiences: Audience[];
}
