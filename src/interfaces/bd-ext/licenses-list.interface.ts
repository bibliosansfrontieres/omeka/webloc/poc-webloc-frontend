import { License } from "./license.interface";

export interface LicensesList {
  iso: string;
  licenses: License[];
}
