import { Language } from "./language.interface";

export interface LanguageList {
  languages: Language[];
  special_languages: Language[];
}
