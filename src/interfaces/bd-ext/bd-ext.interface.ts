export interface BdExt {
  iso: string;
  collections: BdExtLevel1[];
  locations: BdExtLevel1[];
  dates: BdExtLevel1[];
}

export interface BdExtLevel {
  name: string;
  title: string;
}

export interface BdExtLevel1 extends BdExtLevel {
  level2?: BdExtLevel2[];
}

export interface BdExtLevel2 extends BdExtLevel {
  level3?: BdExtLevel3[];
}

export interface BdExtLevel3 extends BdExtLevel {
  level4?: BdExtLevel[];
}
