export interface Language {
  iso: string;
  name: string;
}
