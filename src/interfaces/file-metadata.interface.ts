import { LocalFile } from "./local-file.interface";

export interface FileMetadata {
  [key: string]: any;
  id: number;
  localFile: LocalFile;
  title: string;
  description: string;
  collection2: string;
  collection: string;
  creator: string;
  publisher: string;
  dateCreated: string;
  language: string;
  license: string;
  source: string;
  audience: string;
  subtitles: string;
  duration: string;
  size: string;
  ignore: boolean;
  tag11: string;
  tag12: string;
  tag13: string;
  tag21: string;
  tag22: string;
  tag23: string;
  temporality1: string;
  temporality2: string;
  geography1: string;
  geography2: string;
  package: string;
}
