export interface Package {
  id: number;
  uniqueId: string;
  name: string;
  description: string;
  index: number;
  goal: string;
  collection: string;
  audience: string;
  language: string;
}
