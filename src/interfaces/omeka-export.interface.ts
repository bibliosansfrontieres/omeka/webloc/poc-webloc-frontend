export interface OmekaExport {
  id: number;
  timestamp: number;
  ready: boolean;
  packages: number;
  email: string;
}
