import { LocalFile } from "./local-file.interface";
import { Save } from "./save.interface";

export interface LocalFolder {
  id: number;
  changeDetected: boolean;
  localFiles: LocalFile[];
  saves: Save[];
  name: string;
  uniqueId: string;
  timeLastModified: string;
  serverRelativeUrl: string;
}
