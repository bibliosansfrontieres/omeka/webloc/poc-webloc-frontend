import { FileMetadata } from "./file-metadata.interface";
import { LocalFolder } from "./local-folder.interface";
import { Package } from "./package.interface";

export interface Save {
  id: number;
  name: string;
  iso: string;
  timestamp: string;
  fileMetadatas?: FileMetadata[];
  packages?: Package[];
  email: string | null;
  localFolder?: LocalFolder;
}
