export interface FolderStatus {
  status: "ready" | "not-ready";
  localFiles: { id: number; status: "ready" | "not-ready" }[];
}
