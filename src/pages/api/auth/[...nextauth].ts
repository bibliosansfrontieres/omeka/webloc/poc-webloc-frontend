import AzureADProvider from "next-auth/providers/azure-ad";
import NextAuth, { NextAuthOptions } from "next-auth";

const clientId = process.env.AZURE_AD_CLIENT_ID || "";
const clientSecret = process.env.AZURE_AD_CLIENT_SECRET || "";
const tenantId = process.env.AZURE_AD_TENANT_ID || "";

export const authOptions: NextAuthOptions = {
  providers: [
    AzureADProvider({
      clientId,
      clientSecret,
      tenantId,
    }),
  ],
  pages: {
    signIn: "/signin",
    signOut: "/signout",
  },
};

export default NextAuth(authOptions);
