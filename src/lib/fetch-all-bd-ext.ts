import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { api } from "@/utils/api";

export async function fetchAllBdExt(): Promise<BdExt[]> {
  const result = await api(`/bd-ext/all`, false, {
    cache: "no-store",
  });
  if (!result.ok) throw new Error("Couldn't get all BdExt !");
  return result.json();
}
