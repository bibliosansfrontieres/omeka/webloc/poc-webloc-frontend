import { api } from "@/utils/api";
import { Session } from "next-auth";

export async function postOmekaExport(
  exportItems: { package_id: string; [key: string]: string }[],
  exportProjects: { project_id: string; [key: string]: string }[],
  session: Session | null
): Promise<{ id: string }> {
  const result = await api(`/omeka-export`, false, {
    cache: "no-store",
    method: "post",
    body: JSON.stringify({
      email: session?.user?.email,
      packages: exportItems,
      projects: exportProjects,
    }),
    headers: {
      "Content-type": "application/json",
    },
  });
  if (!result.ok) throw new Error("Couldn't ask Omeka export !");
  return result.json() as Promise<{ id: string }>;
}
