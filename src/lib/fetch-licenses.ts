import { License } from "@/interfaces/bd-ext/license.interface";
import { LicensesList } from "@/interfaces/bd-ext/licenses-list.interface";
import { api } from "@/utils/api";

export async function fetchLicenses(): Promise<License[]> {
  const result = await api(`/bd-ext/licenses`, true, {
    cache: "no-store",
  });
  if (!result.ok) throw new Error("Couldn't get licenses !");
  const res = (await result.json()) as LicensesList;
  return res.licenses;
}
