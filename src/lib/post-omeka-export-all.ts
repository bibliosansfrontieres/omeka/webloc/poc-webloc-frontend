import { api } from "@/utils/api";
import { Session } from "next-auth";

export async function postOmekaExportAll(
  session: Session | null
): Promise<{ id: string }> {
  const result = await api(`/omeka-export/all`, false, {
    cache: "no-store",
    method: "post",
    body: JSON.stringify({
      email: session?.user?.email,
    }),
    headers: {
      "Content-type": "application/json",
    },
  });
  if (!result.ok) throw new Error("Couldn't ask Omeka export all !");
  return result.json() as Promise<{ id: string }>;
}
