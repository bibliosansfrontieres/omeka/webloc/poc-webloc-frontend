import { Audience } from "@/interfaces/bd-ext/audience.interface";
import { AudiencesList } from "@/interfaces/bd-ext/audiences-list.interface";
import { api } from "@/utils/api";

export async function fetchAudiences(iso: string): Promise<Audience[]> {
  const result = await api(`/bd-ext/audiences/${iso}`, true, {
    cache: "no-store",
  });
  if (!result.ok) throw new Error("Couldn't get audiences !");
  const res = (await result.json()) as AudiencesList;
  return res.audiences;
}
