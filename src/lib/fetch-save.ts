import { Save } from "@/interfaces/save.interface";
import { api } from "@/utils/api";

export async function fetchSave(id: number): Promise<Save> {
  const result = await api(`/save/${id}`, false, { cache: "no-store" });
  if (!result.ok) throw new Error("Couldn't find save !");
  return result.json();
}
