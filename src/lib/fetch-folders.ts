import { LocalFolder } from "@/interfaces/local-folder.interface";
import { api } from "@/utils/api";

export async function fetchFolders(): Promise<LocalFolder[]> {
  const result = await api("/local-folder/all", false, { cache: "no-store" });
  if (!result.ok) throw new Error("Couldn't find folders !");
  return result.json();
}
