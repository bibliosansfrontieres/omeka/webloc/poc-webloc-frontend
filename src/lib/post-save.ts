import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { PackageDto } from "@/dtos/package.dto";
import { LocalFolder } from "@/interfaces/local-folder.interface";
import { Save } from "@/interfaces/save.interface";
import { api } from "@/utils/api";

export async function postSave(
  name: string,
  iso: string,
  folder: LocalFolder,
  fileMetadatas: FileMetadataDto[],
  packagesDto: PackageDto[],
  email: string | undefined = undefined
): Promise<Save> {
  const packages = packagesDto.map((pack, i) => {
    return { ...pack, index: i };
  });
  const data = {
    folderId: folder.id,
    iso,
    name,
    fileMetadatas,
    packages,
    email,
  };
  const result = await api(`/save`, false, {
    cache: "no-store",
    method: "post",
    body: JSON.stringify(data),
    headers: {
      "Content-type": "application/json",
    },
  });
  if (!result.ok) throw new Error("Couldn't post save !");
  return result.json();
}
