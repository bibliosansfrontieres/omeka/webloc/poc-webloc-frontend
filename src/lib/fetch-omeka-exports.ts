import { OmekaExport } from "@/interfaces/omeka-export.interface";
import { api } from "@/utils/api";

export async function fetchOmekaExports(
  page: number = 1
): Promise<{ page: number; pages: number; exports: OmekaExport[] }> {
  const result = await api(`/omeka-export/${page}`, false, {
    cache: "no-store",
  });
  if (!result.ok) throw new Error("Couldn't get exports !");
  const res = (await result.json()) as {
    page: number;
    pages: number;
    exports: OmekaExport[];
  };
  return res;
}
