import { LanguageList } from "@/interfaces/bd-ext/languages-list.interface";
import { api } from "@/utils/api";

export async function fetchLanguages(): Promise<LanguageList> {
  const result = await api(`/bd-ext/languages`, true, {
    cache: "no-store",
  });
  if (!result.ok) throw new Error("Couldn't get languages !");
  const res = (await result.json()) as LanguageList;
  return res;
}
