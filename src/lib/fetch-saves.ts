import { Save } from "@/interfaces/save.interface";
import { api } from "@/utils/api";

export async function fetchSaves(folderId: string): Promise<Save[]> {
  const result = await api(`/save/folder/${folderId}`, false, {
    cache: "no-store",
  });
  if (!result.ok) throw new Error("Couldn't find saves !");
  return result.json();
}
