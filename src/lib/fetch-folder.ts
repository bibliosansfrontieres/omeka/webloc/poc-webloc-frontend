import { LocalFolder } from "@/interfaces/local-folder.interface";
import { api } from "@/utils/api";

export async function fetchFolder(
  id: number,
  email: string
): Promise<LocalFolder> {
  const result = await api(`/local-folder/${id}?email=${email}`, true, {
    cache: "no-store",
  });
  if (!result.ok) throw new Error("Couldn't find folder !");
  return result.json();
}
