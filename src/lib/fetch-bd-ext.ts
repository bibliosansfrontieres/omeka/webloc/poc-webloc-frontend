import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";
import { api } from "@/utils/api";

export async function fetchBdExt(iso: string): Promise<BdExt> {
  const result = await api(`/bd-ext/full/${iso}`, false, {
    cache: "no-store",
  });
  if (!result.ok) throw new Error("Couldn't get BdExt !");
  return result.json();
}
