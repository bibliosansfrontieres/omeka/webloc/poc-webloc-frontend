import { Save } from "@/interfaces/save.interface";
import { api } from "@/utils/api";

export async function fetchExports(): Promise<Save[]> {
  const result = await api(`/save/exports`, false, {
    cache: "no-store",
  });
  if (!result.ok) throw new Error("Couldn't get exports !");
  const res = (await result.json()) as Save[];
  return res;
}
