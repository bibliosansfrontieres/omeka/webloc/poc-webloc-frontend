import { api } from "@/utils/api";

export async function postLogin(
  username: string,
  password: string
): Promise<{ id: string; name: string } | null> {
  const result = await api(`/auth/login`, false, {
    cache: "no-store",
    method: "post",
    body: JSON.stringify({ username, password }),
    headers: {
      "Content-type": "application/json",
    },
  });
  if (result.status === 403) return null;
  if (!result.ok) throw new Error("Couldn't post login !");
  return result.json();
}
