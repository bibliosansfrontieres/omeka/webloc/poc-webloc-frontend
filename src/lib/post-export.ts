import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { PackageDto } from "@/dtos/package.dto";
import { LocalFolder } from "@/interfaces/local-folder.interface";
import { api } from "@/utils/api";

export async function postExport(
  folderId: number,
  iso: string,
  fileMetadatas: FileMetadataDto[],
  packagesDto: PackageDto[],
  email: string
): Promise<LocalFolder | undefined> {
  const packages = packagesDto.map((pack, i) => {
    return { ...pack, index: i };
  });
  const data = {
    name: "EXPORT",
    iso,
    folderId,
    fileMetadatas,
    packages,
    email,
  };
  const result = await api(`/local-folder/export/${folderId}`, false, {
    cache: "no-store",
    method: "post",
    body: JSON.stringify(data),
    headers: {
      "Content-type": "application/json",
    },
  });
  if (!result.ok) return undefined;
  return result.json();
}
