import { CanExport } from "@/interfaces/can-export.interface";
import { api } from "@/utils/api";

export async function fetchCanExport(folderId: number): Promise<CanExport> {
  const result = await api(`/local-folder/canExport/${folderId}`, false, {
    cache: "no-store",
  });
  if (!result.ok) throw new Error("Couldn't check canExport !");
  return result.json();
}
