import { Package } from "@/interfaces/package.interface";
import { v4 as uuid } from "uuid";

export class PackageDto {
  [key: string]: string;
  id: string;
  name: string;
  description: string;
  goal: string;
  collection: string;
  audience: string;
  language: string;

  constructor(iso: string, pack?: Package) {
    this.id = pack?.uniqueId || uuid();
    this.name = pack?.name || "";
    this.description = pack?.description || "";
    this.goal = pack?.goal || "";
    this.collection = pack?.collection || "";
    this.audience = pack?.audience || "";
    this.language = pack?.language || iso;
  }
}
