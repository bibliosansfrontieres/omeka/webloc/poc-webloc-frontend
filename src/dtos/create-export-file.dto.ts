import { ItemType } from "@/interfaces/item-type.enum";
import { FileMetadataDto } from "./file-metadata.dto";
import { PackageDto } from "./package.dto";
import { BdExt } from "@/interfaces/bd-ext/bd-ext.interface";

export class CreateExportFileDto {
  // From backend
  type: string;
  format: string;
  extension: string;

  // Metadata
  title: string;
  description: string;
  creator: string;
  publisher: string;
  dateCreated: string;
  language: string;
  license: string;
  source: string;
  audience: string;
  collection: string;
  size: string;
  duration: string;
  subtitles: string;
  package: string;
  pages: string;

  // Auto filled metadata
  subject: string;
  url: string;
  tags: string;

  constructor(file: FileMetadataDto, bdExt: BdExt, iso: string) {
    // From backend
    this.type = file.localFile.type;
    this.format = file.localFile.format;
    this.extension = file.localFile.extension;

    // Metadata
    this.title = file.title;
    this.description = file.description;
    this.creator = file.creator;
    this.publisher = file.publisher;
    this.dateCreated = file.dateCreated;
    this.language = file.language;
    this.license = file.license;
    this.source = file.source;
    this.audience = file.audience;
    this.collection = file.collection;
    if (file.localFile.type === ItemType.TEXT) {
      this.pages = `${file.size}P`;
      this.size = "";
    } else {
      this.pages = "";
      this.size = `${file.size} Mb`;
    }
    this.duration = file.duration;
    this.subtitles = file.subtitles;
    this.package = file.package;

    // Autoo filled metadata
    this.url = `${process.env.PUBLIC_API_URL}/local-file/${file.localFile.uniqueId}.${file.localFile.extension}`;
    this.subject = file.collection;
    const tags: string[] = [];
    if (file.tag11 !== "") tags.push(file.tag11);
    if (file.tag12 !== "") tags.push(file.tag12);
    if (file.tag13 !== "") tags.push(file.tag13);
    if (file.collection2 !== "") tags.push(file.collection2);
    if (file.tag21 !== "") tags.push(file.tag21);
    if (file.tag22 !== "") tags.push(file.tag22);
    if (file.tag23 !== "") tags.push(file.tag23);
    if (file.geography1 !== "") tags.push(file.geography1);
    if (file.geography2 !== "") tags.push(file.geography2);
    if (file.temporality1 !== "") tags.push(file.temporality1);
    if (file.temporality2 !== "") tags.push(file.temporality2);
    if (iso !== "eng") {
      const collection = bdExt.collections.find(
        (collection) => collection.name === file.collection
      );
      if (collection) {
        tags.push(collection.title);
        if (file.tag11 !== "" && collection.level2) {
          const level2 = collection.level2.find(
            (lvl2) => lvl2.name === file.tag11
          );
          if (level2) {
            tags.push(level2.title);
            if (file.tag12 !== "" && level2.level3) {
              const level3 = level2.level3.find(
                (lvl3) => lvl3.name === file.tag12
              );
              if (level3) {
                tags.push(level3.title);
                if (file.tag13 !== "" && level3.level4) {
                  const level4 = level3.level4.find(
                    (lvl4) => lvl4.name === file.tag13
                  );
                  if (level4) {
                    tags.push(level4.title);
                  }
                }
              }
            }
          }
        }
      }
      if (file.collection2 !== "") {
        const collection2 = bdExt.collections.find(
          (collection) => collection.name === file.collection2
        );
        if (collection2) {
          tags.push(collection2.title);
          if (file.tag21 !== "" && collection2.level2) {
            const level2 = collection2.level2.find(
              (lvl2) => lvl2.name === file.tag21
            );
            if (level2) {
              tags.push(level2.title);
              if (file.tag22 !== "" && level2.level3) {
                const level3 = level2.level3.find(
                  (lvl3) => lvl3.name === file.tag22
                );
                if (level3) {
                  tags.push(level3.title);
                  if (file.tag23 !== "" && level3.level4) {
                    const level4 = level3.level4.find(
                      (lvl4) => lvl4.name === file.tag23
                    );
                    if (level4) {
                      tags.push(level4.title);
                    }
                  }
                }
              }
            }
          }
        }
        if (file.temporality1 !== "") {
          const date = bdExt.dates.find(
            (date) => date.name === file.temporality1
          );
          if (date) {
            tags.push(date.title);
            if (file.temporality2 !== "" && date.level2) {
              const level2 = date.level2.find(
                (lvl2) => lvl2.name === file.temporality2
              );
              if (level2) {
                tags.push(level2.title);
              }
            }
          }
        }
        if (file.geography1 !== "") {
          const location = bdExt.locations.find(
            (location) => location.name === file.geography1
          );
          if (location) {
            tags.push(location.title);
            if (file.geography2 !== "" && location.level2) {
              const level2 = location.level2.find(
                (lvl2) => lvl2.name === file.geography2
              );
              if (level2) {
                tags.push(level2.title);
              }
            }
          }
        }
      }
    }
    this.tags = tags.join(",");
  }
}
