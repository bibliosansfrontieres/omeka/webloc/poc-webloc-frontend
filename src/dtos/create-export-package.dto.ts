import { PackageDto } from "./package.dto";

export class CreateExportPackageDto {
  id: string;
  name: string;
  description: string;
  goal: string;
  collection: string;
  audience: string;
  language: string;

  constructor(packageDto: PackageDto) {
    this.id = packageDto.id;
    this.name = packageDto.name;
    this.description = packageDto.description;
    this.goal = packageDto.goal;
    this.collection = packageDto.collection;
    this.audience = packageDto.audience;
    this.language = packageDto.language;
  }
}
