import { LocalFile } from "@/interfaces/local-file.interface";

export class FileMetadataDto {
  [key: string]: any;
  localFile: LocalFile;
  title: string;
  description: string;
  creator: string;
  publisher: string;
  dateCreated: string;
  language: string;
  license: string;
  source: string;
  audience: string;
  size: string;
  duration: string;
  subtitles: string;
  collection: string;
  tag11: string;
  tag12: string;
  tag13: string;
  collection2: string;
  tag21: string;
  tag22: string;
  tag23: string;
  ignore: boolean;
  package: string;
  temporality1: string;
  temporality2: string;
  geography1: string;
  geography2: string;


  constructor(localFile: LocalFile) {
    this.localFile = localFile;
    this.title = "";
    this.description = "";
    this.creator = "";
    this.publisher = "";
    this.dateCreated = "";
    this.language = "";
    this.license = "";
    this.source = "";
    this.audience = "";
    this.size = localFile.size || "";
    this.duration = localFile.duration ? localFile.duration.toString() : "";
    this.subtitles = "";
    this.collection = "";
    this.tag11 = "";
    this.tag12 = "";
    this.tag13 = "";
    this.tag21 = "";
    this.tag22 = "";
    this.tag23 = "";
    this.collection2 = "";
    this.ignore = false;
    this.package = "";
    this.temporality1 = "";
    this.temporality2 = "";
    this.geography1 = "";
    this.geography2 = "";
  }
}
