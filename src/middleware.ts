import { getToken } from "next-auth/jwt";
import { NextRequest, NextResponse } from "next/server";

export async function middleware(request: NextRequest) {
  const url = new URL(request.url);

  if (
    url.pathname === "/" ||
    url.pathname.startsWith("/folder/") ||
    url.pathname.startsWith("/omeka-export") ||
    url.pathname === "/signout"
  ) {
    const token = await getToken({ req: request });
    if (!token) {
      return NextResponse.redirect(new URL("/signin", request.url));
    }
  }

  if (url.pathname === "/signin") {
    const token = await getToken({ req: request });
    if (token) {
      return NextResponse.redirect(new URL("/", request.url));
    }
  }

  return NextResponse.next();
}
