import SignInForm from "@/components/SignInForm";

export const metadata = {
  title: "WebLOC - Signin",
};

export default function SignIn() {
  return (
    <main className="form-signin w-200 m-auto text-center">
      <SignInForm />
    </main>
  );
}
