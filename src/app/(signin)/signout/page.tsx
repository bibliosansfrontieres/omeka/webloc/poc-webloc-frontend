import SignOutForm from "@/components/SignOutForm";

export const metadata = {
  title: "WebLOC - Signout",
};

export default function SignOut() {
  return (
    <main className="form-signin w-200 m-auto text-center">
      <SignOutForm />
    </main>
  );
}
