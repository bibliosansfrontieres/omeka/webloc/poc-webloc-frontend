import { fetchFolder } from "@/lib/fetch-folder";
import { notFound } from "next/navigation";
import { Audience } from "@/interfaces/bd-ext/audience.interface";
import { License } from "@/interfaces/bd-ext/license.interface";
import { fetchAudiences } from "@/lib/fetch-audiences";
import { fetchLanguages } from "@/lib/fetch-languages";
import { fetchLicenses } from "@/lib/fetch-licenses";
import AutoSaveWrapper from "@/components/folderPath/AutoSaveWrapper";
import { getServerSession } from "next-auth";
import { LanguageList } from "@/interfaces/bd-ext/languages-list.interface";
import { Metadata } from "next";

type PageProps = {
  params: {
    folderId: string;
  };
};

export async function generateMetadata({
  params,
}: PageProps): Promise<Metadata> {
  const session = await getServerSession();
  if (!session || !session.user || !session.user.email) notFound();
  const folder = await fetchFolder(+params.folderId, session.user.email);
  return {
    title: `WebLOC - ${folder.name}`,
  };
}

export default async function FilesPage({ params }: PageProps) {
  const session = await getServerSession();
  if (!session || !session.user || !session.user.email) notFound();
  const folder = await fetchFolder(+params.folderId, session.user.email);
  if (!folder) notFound();
  const audiences: Audience[] = await fetchAudiences("eng");
  const languages: LanguageList = await fetchLanguages();
  const licenses: License[] = await fetchLicenses();

  return (
    <main className="container-fluid my-5">
      <AutoSaveWrapper
        audiences={audiences}
        languages={languages}
        licenses={licenses}
        folder={folder}
      />
    </main>
  );
}
