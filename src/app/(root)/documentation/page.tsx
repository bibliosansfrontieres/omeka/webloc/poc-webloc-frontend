import ExportList from "@/components/exports/ExportList";

export const metadata = {
  title: "WebLOC - Documentation",
};

export default async function DocumentationPage() {
  return (
    <main className="container my-5">
      <h1>Documentation of WebLOC</h1>
      <p className="mt-5">
        You can find here the documentation that explain how to use WebLOC.
      </p>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          maxWidth: "500px",
          margin: "3em auto",
        }}
      >
        <a
          target="_blank"
          href="https://bibliosansfrontieresorg.sharepoint.com/:w:/r/sites/bibliosansfrontieres.org/_layouts/15/Doc.aspx?sourcedoc=%7B0F24E9E8-95D9-4A14-BC89-88CC344F8660%7D&file=WebLOC%20Documentation%20English.docx&action=default&mobileredirect=true"
          className="btn btn-primary btn-lg"
        >
          English
        </a>
        <a
          target="_blank"
          href="https://bibliosansfrontieresorg.sharepoint.com/:w:/r/sites/bibliosansfrontieres.org/_layouts/15/Doc.aspx?sourcedoc=%7BD560CECB-7DF5-4994-A54C-61C4BD9AFD1C%7D&file=Documentation%20WebLOC%20Fench.docx&action=default&mobileredirect=true"
          className="btn btn-primary btn-lg"
        >
          French
        </a>
      </div>
    </main>
  );
}
