export default function Head() {
  return (
    <>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta name="description" content="LOC replacement application" />
      <link rel="icon" href="/favicon.ico" />
    </>
  );
}
