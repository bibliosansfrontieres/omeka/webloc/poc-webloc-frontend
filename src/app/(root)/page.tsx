import SelectFolder from "@/components/SelectFolder";

export const metadata = {
  title: "WebLOC - Home",
};

export default async function Home() {
  return (
    <main className="container my-5">
      <h1>Select a project folder</h1>
      <p>
        Here is the list of the folders created in SharePoint.
        <br />
        Each folder represent a group of files to import.
      </p>
      <p>
        In order to create a folder, you need to go to the{" "}
        <a
          href="https://bibliosansfrontieresorg.sharepoint.com/sites/WebLoc/Documents%20partages/Forms/AllItems.aspx"
          target="_blank"
        >
          WebLOC SharePoint
        </a>
        .
        <br />
        Go into &quot;IMPORT&quot; folder and then, you can create a folder with
        the name that you want.
      </p>
      <p>Please select the folder of your project :</p>
      <SelectFolder />
    </main>
  );
}
