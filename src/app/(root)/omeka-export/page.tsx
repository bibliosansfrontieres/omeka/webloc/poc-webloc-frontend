import OmekaExport from "@/components/omekaExports/OmekaExport";

export const metadata = {
  title: "WebLOC - Omeka Export",
};

export default function OmekaExportPage() {
  return (
    <main className="container my-5 text-center">
      <OmekaExport />
    </main>
  );
}
