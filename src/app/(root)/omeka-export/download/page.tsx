import OmekaExportDownload from "@/components/omekaExports/OmekaExportDownload";

export const metadata = {
  title: "WebLOC - Omeka Export",
};

export default function OmekaExportDownloadPage() {
  return (
    <main className="container my-5 text-center">
      <OmekaExportDownload />
    </main>
  );
}
