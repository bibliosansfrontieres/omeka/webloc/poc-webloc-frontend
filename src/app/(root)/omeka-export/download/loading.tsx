import Menu from "@/components/omekaExports/Menu";
import Pagination from "@/components/omekaExports/download/Pagination";

export default function LoadingOmekaExportDownloadPage() {
  return (
    <main className="container my-5 text-center">
      <Menu />
      <table className="table">
        <thead>
          <tr>
            <td>ID</td>
            <td>Date</td>
            <td>User</td>
            <td>Status</td>
            <td>Download</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan={100}>Loading...</td>
          </tr>
        </tbody>
      </table>
      <Pagination pages={0} page={0} />
    </main>
  );
}
