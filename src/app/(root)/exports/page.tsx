import ExportList from "@/components/exports/ExportList";

export const metadata = {
  title: "WebLOC - Exports",
};

export default async function Home() {
  return (
    <main className="container my-5">
      <ExportList />
    </main>
  );
}
