export default function Loading() {
  return (
    <main className="container my-5 text-center">
      <p>Loading...</p>
    </main>
  );
}
