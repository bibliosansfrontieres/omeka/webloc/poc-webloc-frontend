export function api(
  endpoint: string,
  local?: boolean,
  init?: RequestInit | undefined
): Promise<Response> {
  local = local !== undefined ? local : true;
  const apiUrl = local ? process.env.LOCAL_API_URL : process.env.PUBLIC_API_URL || "http://localhost:3000";
  return fetch(`${apiUrl}${endpoint}`, init);
}
