import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { PackageDto } from "@/dtos/package.dto";
import { ItemType } from "@/interfaces/item-type.enum";

// Types

export type ValidationResult = {
  valid: boolean;
  error?: string;
};

export type ColumnValidation = {
  name: string;
  required?: boolean;
  isValid?: (value: string, file: FileMetadataDto) => ValidationResult;
};

// Validation

const yearsRegex = /^[0-9?]{4}$/;
const numberRegex = /^[0-9]*$/;
const numberWithDotRegex = /[0-9+.]*$/;
const forbiddenCharsRegex = /[^\u0000-\u001F\u0020-\u1EFF]|[|&]/;
const httpRegex =
  /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/;
const timeRegex = /^\d{2,}:\d{2}:\d{2}$/;

const notRequiredIsValid = (value: string): ValidationResult => {
  const notUsingForbiddenChars = isNotUsingForbiddenChars(value);
  if (!notUsingForbiddenChars.valid) return notUsingForbiddenChars;
  return { valid: true };
};

const requiredIsValid = (value: string): ValidationResult => {
  const notRequiredIsValidResult = notRequiredIsValid(value);
  if (!notRequiredIsValidResult.valid) return notRequiredIsValidResult;
  const notEmpty = isNotEmpty(value);
  if (!notEmpty.valid) return notEmpty;
  return { valid: true };
};

const isNotEmpty = (value: string): ValidationResult => {
  if (value === "") return { valid: false, error: `Field is required` };
  return { valid: true };
};

const isNotUsingForbiddenChars = (value: string): ValidationResult => {
  const match = value.match(forbiddenCharsRegex);
  if (match !== null)
    return { valid: false, error: `Forbidden chars used (${match.join("")})` };
  return { valid: true };
};

export const columnsValidations: ColumnValidation[] = [
  {
    name: "title",
    isValid: requiredIsValid,
  },
  {
    name: "description",
    isValid: requiredIsValid,
  },
  {
    name: "creator",
    isValid: requiredIsValid,
  },
  {
    name: "publisher",
    isValid: notRequiredIsValid,
  },
  {
    name: "dateCreated",
    isValid: (value: string) => {
      const defaultValid = requiredIsValid(value);
      if (!defaultValid.valid) return defaultValid;
      if (value.length !== 4) {
        return { valid: false, error: "Year needs to be in 4 characters !" };
      } else if (
        !value.includes("?") &&
        parseInt(value) > new Date().getFullYear()
      ) {
        return { valid: false, error: "Year doesn't exists yet !" };
      } else if (!yearsRegex.test(value)) {
        return { valid: false, error: "Not good format for year !" };
      } else if (parseInt(value.slice(0, 2)) > 20) {
        return { valid: false, error: "Year doesn't exists yet !" };
      }
      return { valid: true };
    },
  },
  {
    name: "language",
    isValid: requiredIsValid,
  },
  {
    name: "license",
    isValid: requiredIsValid,
  },
  {
    name: "source",
    isValid: (value: string) => {
      const defaultValid = notRequiredIsValid(value);
      if (!defaultValid.valid) return defaultValid;
      if (value !== "") {
        if (!httpRegex.test(value))
          return { valid: false, error: "This field needs to be a valid URL" };
      }
      return { valid: true };
    },
  },
  {
    name: "audience",
    isValid: requiredIsValid,
  },
  {
    name: "collection",
    isValid: requiredIsValid,
  },
  {
    name: "collection2",
    isValid: notRequiredIsValid,
  },
  {
    name: "size",
    isValid: (value, file) => {
      const defaultValid = requiredIsValid(value);
      if (!defaultValid.valid) return defaultValid;
      if (file.localFile.type === ItemType.TEXT) {
        if (!numberRegex.test(value))
          return { valid: false, error: "Must be a number" };
      } else {
        if (value.includes(","))
          return {
            valid: false,
            error: '"," is forbidden, use "." instead',
          };
        if (!numberWithDotRegex.test(value))
          return { valid: false, error: "Must be a number" };
        const multipleDot = value.split(".");
        if (multipleDot.length > 2)
          return { valid: false, error: "This is not a number !" };
      }
      return { valid: true };
    },
  },
  {
    name: "duration",
    isValid: (value) => {
      const defaultValid = requiredIsValid(value);
      if (!defaultValid.valid) return defaultValid;
      if (!timeRegex.test(value))
        return { valid: false, error: "Must be formated like 00:00:00" };
      return { valid: true };
    },
  },
  {
    name: "subtitles",
    isValid: notRequiredIsValid,
  },
  {
    name: "tag11",
    isValid: notRequiredIsValid,
  },
  {
    name: "tag12",
    isValid: notRequiredIsValid,
  },
  {
    name: "tag13",
    isValid: notRequiredIsValid,
  },
  {
    name: "tag21",
    isValid: notRequiredIsValid,
  },
  {
    name: "tag22",
    isValid: notRequiredIsValid,
  },
  {
    name: "tag23",
    isValid: notRequiredIsValid,
  },
  {
    name: "temporality1",
    isValid: notRequiredIsValid,
  },
  {
    name: "temporality2",
    isValid: notRequiredIsValid,
  },
  {
    name: "geography1",
    isValid: notRequiredIsValid,
  },
  {
    name: "geography2",
    isValid: notRequiredIsValid,
  },
  {
    name: "package",
    isValid: notRequiredIsValid,
  },
];

export function isValid(name: string, value: string, file: FileMetadataDto) {
  const columnOption = columnsValidations.find(
    (option) => option.name === name
  );
  if (!columnOption) return notRequiredIsValid(value);
  if (!columnOption.isValid) return { valid: true };
  return columnOption.isValid(value, file);
}
