import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { PackageDto } from "@/dtos/package.dto";
import { ItemType } from "@/interfaces/item-type.enum";

// Types

export type ValidationResult = {
  valid: boolean;
  error?: string;
};

export type ColumnValidation = {
  name: string;
  required?: boolean;
  isValid?: (value: string, packageDto: PackageDto) => ValidationResult;
};

// Validation

const yearsRegex = /^[0-9?]{4}$/;
const numberRegex = /^[0-9]*$/;
const numberWithDotRegex = /[0-9+.]*$/;
const forbiddenCharsRegex = /[^\u0000-\u001F\u0020-\u1EFF]|[|&]/;
const httpRegex =
  /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/;
const timeRegex = /^\d{2,}:\d{2}:\d{2}$/;

const notRequiredIsValid = (value: string): ValidationResult => {
  const notUsingForbiddenChars = isNotUsingForbiddenChars(value);
  if (!notUsingForbiddenChars.valid) return notUsingForbiddenChars;
  return { valid: true };
};

const requiredIsValid = (value: string): ValidationResult => {
  const notRequiredIsValidResult = notRequiredIsValid(value);
  if (!notRequiredIsValidResult.valid) return notRequiredIsValidResult;
  const notEmpty = isNotEmpty(value);
  if (!notEmpty.valid) return notEmpty;
  return { valid: true };
};

const isNotEmpty = (value: string): ValidationResult => {
  if (value === "") return { valid: false, error: `Field is required` };
  return { valid: true };
};

const isNotUsingForbiddenChars = (value: string): ValidationResult => {
  const match = value.match(forbiddenCharsRegex);
  if (match !== null)
    return { valid: false, error: `Forbidden chars used (${match.join("")})` };
  return { valid: true };
};

export const packagesColumnsValidation: ColumnValidation[] = [
  {
    name: "name",
    isValid: requiredIsValid,
  },
];

export function isPackageValid(
  name: string,
  value: string,
  packageDto: PackageDto
) {
  const columnOption = packagesColumnsValidation.find(
    (option) => option.name === name
  );
  if (!columnOption) return notRequiredIsValid(value);
  if (!columnOption.isValid) return { valid: true };
  return columnOption.isValid(value, packageDto);
}
