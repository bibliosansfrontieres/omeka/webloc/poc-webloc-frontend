export const ColumnsTransformation = {
  // Dublin Core
  title: "Dublin Core:Title",
  description: "Dublin Core:Description",
  creator: "Dublin Core:Creator",
  publisher: "Dublin Core:Publisher",
  dateCreated: "Dublin Core:Date Created",
  language: "Dublin Core:Language",
  license: "Dublin Core:License",
  source: "Dublin Core:Source",
  audience: "Dublin Core:Audience",
  subject: "Dublin Core:Subject",
  extension: "Dublin Core:Format",
  format: "Dublin Core:Type",

  // Technical
  collection: "Technical:Collection",
  type: "Technical:Item type",

  // Item Type Metadata
  pages: "Item Type Metadata:Pages",
  size: "Item Type Metadata:Size (Mb)",
  duration: "Item Type Metadata:Duration",
  subtitles: "Item Type Metadata:Subtitles",

  // Other
  tags: "TAGS",
  url: "URL",
  package: "Item Type Metadata:PackageFilter",
};
