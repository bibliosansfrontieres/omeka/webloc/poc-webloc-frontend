import { FileMetadataDto } from "@/dtos/file-metadata.dto";
import { ItemType } from "@/interfaces/item-type.enum";

export type ColumnDisabling = {
  name: string;
  isDisabled: (file: FileMetadataDto) => boolean;
};

export const columnsDisabling: ColumnDisabling[] = [
  {
    name: "duration",
    isDisabled: (file: FileMetadataDto) => {
      return file.localFile.type === ItemType.TEXT;
    },
  },
  {
    name: "subtitles",
    isDisabled: (file: FileMetadataDto) => {
      return file.localFile.type !== ItemType.MOVING_IMAGE;
    },
  },
];

export function isColumnDisabled(name: string, file: FileMetadataDto) {
  let isDisabled = false;
  const columnDisabling = columnsDisabling.find(
    (columnDisabling) => columnDisabling.name === name
  );
  if (columnDisabling) {
    isDisabled = columnDisabling.isDisabled(file);
  }
  return isDisabled;
}
