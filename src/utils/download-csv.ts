export const downloadCsv = (output: string, name: string) => {
  const fileName = `${name}.csv`;
  const file = new Blob([output], { type: "text/csv" });
  const element = document.createElement("a");
  element.href = URL.createObjectURL(file);
  element.download = fileName;
  document.body.appendChild(element);
  element.click();
};
