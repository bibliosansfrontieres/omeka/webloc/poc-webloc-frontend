import csvtojson from "csvtojson";

export async function csvToObject<T>(input: string): Promise<T[]> {
  const objs = await new Promise((resolve) => {
    csvtojson()
      .fromString(input)
      .then((result) => {
        resolve(result);
      });
  });
  return objs as T[];
}
